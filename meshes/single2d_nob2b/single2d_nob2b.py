import numpy as np

# mesh file
mesh = open("mesh.x", "w")

# number of blocks
mesh.write("1\n") 

# uniform grid space step
LX =  7    # x length
LY =  5    # y length
N  = 16    # grid cells per unit length
h  =  1/N  # space step

# block 1 range: 0-LX, 0-LY
# plot3D format header: block index range
mesh.write( repr(LX*N+1) + ' ' + repr(LY*N+1)   + ' ' + repr(1) + "\n" ) 

# count every six coordinate then start new line
count = 0

# block 1 x
for j in range(LY*N+1):
    for i in range(LX*N+1):
        mesh.write(repr(i*h) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 1 y
for j in range(LY*N+1):
    for i in range(LX*N+1):
        mesh.write(repr(j*h) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 1 z
for j in range(LY*N+1):
    for i in range(LX*N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

mesh.close()

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

# block 1 face 1,2,4
bcMap.write("FarField 1 1\n")
bcMap.write("FarField 1 2\n")
bcMap.write("FarField 1 4\n")
bcMap.write("FarField 1 5\n")

bcMap.close()
