import numpy as np
import struct

# mesh file
mesh = open("mesh.x", "wb")

# number of blocks
mesh.write(struct.pack('i',1))

# uniform grid space step
LX =  2    # x length
LY =  8    # y length
LZ =  2    # z length
N  =  8   # grid cells per unit length
h  =  1/N  # space step

# block 1 range: 0-LX, 0-LY
# plot3D format header: block index range
mesh.write(struct.pack('iii', LX*N+1, LY*N+1, LZ*N+1))

# write coordinates
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write(struct.pack('d', i*h))
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write(struct.pack('d', j*h))
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write(struct.pack('d', k*h))

mesh.close()

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

# block 1 face 1,2,4,5,6
bcMap.write("ViscousWall 1 1\n")
bcMap.write("ViscousWall 1 2\n")
bcMap.write("ViscousWall 1 3 1 {} {} {}\n".format(LX*N+1, LY//4*N+1, LY*3//4*N+1))
bcMap.write("ViscousWall 1 5\n")
bcMap.write("ViscousWall 1 6\n")

#block 1 face 3
bcMap.write("Block2Block 1 3 1 {} 1 {} 1 3 1 {} {} {} no\n".format(
            LX*N+1, LY//4*N+1, LX*N+1, LY*N+1, LY*3//4*N+1))

bcMap.close()
