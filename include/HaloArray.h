#ifndef HALOARRAY_H
#define HALOARRAY_H

#include <mpi.h>
#include <omp.h>
#include <list>
#include "Block.h"
#include "BlockGraph.h"

class HaloArray
{
  public:  //---------- public member functions ----------//
  HaloArray()
  { f = 0; _sBuf = 0; _ptBg = 0; _tShm = 0.0; \
    _rBuf = 0; _shmemBuf = 0; }
  /* default constructor */ 

  HaloArray(BlockGraph* ptBg, vector<int> bids, int nHalo);

  HaloArray(const HaloArray& rhs);

  const HaloArray& operator=(const HaloArray& rhs);

  ~HaloArray();

  double& at(int iBlk, int i, int j, int k);
  /* return the reference of data at (i,j,k) */

  int block_start(int iBlk)
  /* the start index of the iBlk th data */
  { return(_blkStrts[iBlk]); }

  void setup_timer(int nThrd);

  void collect_time();

  void view_copy_time();

  double time_shmem()
  /* time in copy for shared memory boundary */
  { return(_tShm); }

  int update_halo_begin();
  /* launch all the non-blocking send&recv */

  int update_halo_end();
  /* make sure the non-blocking communication finishes */

  int pack_halo_to_buf();

  int unpack_buf_to_halo();

  int copy_halo_shared_mem();

  int view();
  /* print block and communication info */

  private: //---------- private member functions ---------//
  int _setup_comm();
  /* setup subarray type and mpi parameters */

  int _comm_tag(int bid, int fid, int mapid);

  public:  //---------- public attributes       ----------//
  double *f;    // data

  private: //---------- private attributes      ----------//
  int                    _nh;            // # halo
  BlockGraph*            _ptBg;
  vector<int>            _bids;          // block
  double                *_sBuf, *_rBuf;  // send,recv buf
  vector<int>            _bufPoss;
  double                *_shmemBuf;      // shmem buffer
  vector<MPI_Request>    _reqs;
  vector<int>            _pids;          // target part id
  vector<int>            _sTags, _rTags; // send tags
  vector<int>            _blkStrts;      // block start idx
  int                    _nCopy;
  double                *_cpyTimes;      // time of copy to MPI buffer
  double                 _tShm;          // time of copy for shmem
  int                    _nThrd;         // # openMP thread
  double                 _cpyTimeMax[3], _cpyTimeAvg[3];
  double                 _imblncMax[3],  _imblncAvg[3];
};

#endif
