#ifndef PARTITIONER_H
#define PARTITIONER_H

#include <fstream>
#include "common.h"
#include "BlockGraph.h"
#include "Partition.h"
#include "PartTable.h"
#include "PartMethod.h"
#include "PartCluster.h"
#include "PartGreedZone.h"
#include "PartGreedyCut.h"
#include "PartMultiLevel.h"
#include "PartPureGreedy.h"
#include "PartPureGreedyCut.h"

using namespace std;

class Partitioner
{
  public:  //---------- public member functions ----------//
  Partitioner(int nPart)
  { _nPart = nPart; _parts = PartArray(nPart); \
    _toler = 0.04;  _tbl   = PartTable(nPart);}

  Partitioner() {}

  void get_block_ids(int pid, vector<int>& bids)
  /* get the blocks in the input partition */
  { bids = _parts[pid].blkIDs; }

  void set_tolerance(double val)
  /* set tolerance */
  { _toler = val; }

  void config(const int nPart)
  { _nPart = nPart; _parts = PartArray(nPart); _tbl = PartTable(nPart); }

  void setup_partition_method(Method_t mthdType);
  /* create partition method based on input type */

  void set_part_order(int order)
  /* set order of partition */
  {
    _parts.set_order(order);
  }

  int decompose(BlockGraph& geo, BlockGraph& grid);
  /*----------------------------------------------------------
    Decompose geometry blocks to paritions.
    
    In:  geo  - input geometry blocks
    Out: grid - partitioned grid blocks.

    Pre: Partition method must be setup

    Res: * Each block in grid is assigned with a partition id.
         * Partition array is built.
  ----------------------------------------------------------*/

  int setup_partition(BlockGraph& grid);
  /*----------------------------------------------------------
    Set up the partition's block and neighbor partition.
    
    In:   grid - BlockGraph

    Pre: The block graph is already decomposed, i.e. each block
         has partition id.
  ----------------------------------------------------------*/

  int fwrite_part_time(const string& fname);
  /* write to file the workload of each partition */

  int view_part();
  /* call each part's view() in descending order of workload */

  int view_part(BlockGraph& bg, int pid);
  /* view a part's computation, communication load */

  int eval_part_prop(BlockGraph& bg);
  /* evaluate the imbalance,  # edge-cuts and communication volume */

  int view_part_prop();
  /* print the partition's property */

  int adjust_all(BlockGraph& bg);
  /* adjust partition to make it more balanced */

  int adjust_greedy(BlockGraph& bg);
  /* adjust partition in a greedy way */

  int exchange(BlockGraph& bg);

  private: //----------    private functions    ----------//
  int _setup_exchange_tbl(BlockGraph& bg);

  int _move_blk(BlockGraph& bg, int bid, int pid0, int pid1);

  protected: //-------- protected attributes    ----------//
  int          _nPart;       // # partitions
  PartArray    _parts;       // partitions
  PartTable    _tbl;         // table of partition shifts
  double       _wkAvg;       // average workload of each part
  double       _toler;
  double       _edgeCuts[4]; // 0 sum, 1 max, 2 min, 3 sum ^2
  double       _commVols[4];
  double       _imblncs[4];
  double       _t[4], _tComm[4], _tComp[4];
  Method_t     _mthdType;
  PartMethod*  _ptPart;
  vector<bool> _isMvds;
  double       _directCpyVols[4], _indirectCpyVols[4], _shrVols[4];
  int          _nBlk;
};


#endif
