BEGIN{
  imax = 0;
  max  = 0.0;
}
{
  if (NR > 1) {
    if ($3 > max) {
      max  = $3;
      imax = $1;
    }
  }
}
END{
  printf("%d\t%f\n", imax, max);
}
