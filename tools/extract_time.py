import os
import sys


for name in os.listdir('.'):
  if name[-6:] == 'output':
    times = []
    with open(name) as infile:
      for i, line in enumerate(infile):
        items = line.split()
        if (len(items) > 1):
          if items[0] == 'total':
            times.append(items[4])
          if items[0] == 'comp':
            times.append(items[4])
          if items[0] == 'comm':
            times.append(items[4])
          if items[0] == 'copy':
            times.append(items[4])
          if items[0] == 'shr':
            times.append(items[4])
          if items[0] == 'spin':
            times.append(items[4])
    print(name[:-7] + ',', end='')
    for t in times:
      print(t + ',', end='')
    print('')
