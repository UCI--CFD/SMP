#include "PartCluster.h"

using namespace std;


/************************       ElementHeap Class      ************************/


int ElementHeap::pop()
{
  if(_bids.size() == 0){
    cerr << "ERROR: Try to pop an empty ElementHeap.";
    exit(-1);
  }

  int top = _bids[0];

  swap(_bids[0],     _bids[_bids.size()-1]);
  swap(_msgIncrs[0], _msgIncrs[_bids.size()-1]);
  _bids.pop_back();
  _msgIncrs.pop_back();
  heapify(0);

  return(top);
}


int ElementHeap::pop(int bid)
{
  if(_bids.size() == 0){
    cerr << "ERROR: Try to pop an empty ElementHeap.";
    exit(-1);
  }

  bool         isFound = false;
  unsigned int i       = 0;

  for(i=0; i<_bids.size(); i++){
    if(_bids[i] == bid){
      isFound  = true;
      break;
    }
  }

  if(isFound){
    swap(_bids[i],     _bids[_bids.size()-1]);
    swap(_msgIncrs[i], _msgIncrs[_bids.size()-1]);
    _bids.pop_back();
    _msgIncrs.pop_back();
    heapify(i);
  }
  else{
    cerr << "ERROR: Try to pop a block not in heap." << endl;
    exit(-1);
  }

  return(0);
}


int ElementHeap::heapify(int i)
{
  unsigned int l = 2*i+1, r = 2*(i+1);
  int          iMin = i;
  if(l < _bids.size()){
    if(_msgIncrs[l] < _msgIncrs[iMin])  iMin = (int)l;
  }
  if(r < _bids.size()){
    if(_msgIncrs[r] < _msgIncrs[iMin])  iMin = (int)r;
  }
  if(i != iMin){
    swap(_bids[i],     _bids[iMin]);
    swap(_msgIncrs[i], _msgIncrs[iMin]);
    heapify(iMin);
  }

  return(0);
}


int ElementHeap::find_blk(int bid)
{
  int pos = INDEX_NULL;

  for(unsigned int i=0; i<_bids.size(); i++)
    if(_bids[i] == bid)  pos = i;
    
  return(pos);
}


int ElementHeap::add(int bid, double msgIncr)
{
  _bids.push_back(bid);
  _msgIncrs.push_back(msgIncr);

  int i = _bids.size() - 1; // index of added element

  while(i > 0 && _msgIncrs[i] < _msgIncrs[(i-1)/2]){
    swap(_msgIncrs[i], _msgIncrs[(i-1)/2]);
    swap(_bids[i],     _bids[(i-1)/2]);
    i = (i-1) / 2;
  }

  return(0);
}


int ElementHeap::decr(int i, double val)
{
  _msgIncrs[i] -= val;

  if(val > 0){
    while(i > 0 && _msgIncrs[i] < _msgIncrs[(i-1)/2]){
      swap(_msgIncrs[i], _msgIncrs[(i-1)/2]);
      swap(_bids[i],     _bids[(i-1)/2]);
      i = (i-1) / 2;
    }
  }
  else{
    heapify(i);
  }

  return(0);
}


int ElementHeap::clear()
{
  _bids.clear();
  _msgIncrs.clear();
  return(0);
}


int ElementHeap::view()
{
  for(unsigned int i=0; i<_bids.size(); i++)
    cout << _bids[i] << " " << _msgIncrs[i] << endl;

  return(0);
}


/************************          Funcitons           ************************/


int _cmpr_top(ElementHeap& hp0, ElementHeap& hp1)
{
  if(hp0.size() == 0 || hp1.size() == 0){
    cerr << "ERROR: compare top of empty element heap." << endl;
    exit(-1);
  }

  int res;
  // if top has same msg load
  if(abs(hp0._msgIncrs[0] - hp1._msgIncrs[0]) < DSMALL)
    res = 0;
  // different msg load
  else if(hp0._msgIncrs[0] < hp1._msgIncrs[0])
    res = -1;
  else
    res =  1;

  return(res);
}


/************************       PartCluster Class      ************************/


int PartCluster::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
{
  vector<int> nLoads;     // # avg load assigned to a partition
  int         nPartUsed;  // # used partitions

  // save graph without copying coordinates
  grid = geo;

  // # partition and average workload
  _nPart = nPart;
  _wkAvg = (double)geo.work_load() / _nPart;

  // creat partition array
  _parts = PartArray(_nPart);

  // init partition 0 with all blocks
  for(int i=0; i<grid.size(); i++){
    _parts[0].blkIDs.push_back(i);
    _parts.add_load(0, grid[i].work_load());
    grid[i].set_partition(0);
  }
  // part 0 is used and assigned with work load of all partitions
  nPartUsed = 1; 
  nLoads.push_back(_nPart);
  // set all b2b maps as shared memory map
  _init_shmem_map(grid);

  // decompose with bisection
  while(_tail < _nPart-1){
    // break down to elementary blocks
    int size0 = grid.size();
    grid.cut_to_elements();
      //cut_to_element(grid);
    // set element's partition
    for(int i=size0; i<grid.size(); i++){
      int pid = grid[i].partition_id();
      _parts[pid].blkIDs.push_back(i);
    }
#if verbose>=1
    cout << "PartCluster: ---- Cut into " << grid.size() << " blocks ----" << endl;
#endif
    // mark all blocks as unMoved and set init msg diff as 0
    for(int i=0; i<grid.size(); i++){
      _isMvds.push_back(false);
      _msgDiffs0.push_back(0.0);
      _msgDiffs1.push_back(0.0);
    }
    // bisect partition that already has blocks
    for(int i=0; i<nPartUsed; i++){
      if(nLoads[i] <= 1)  continue;
      _tail++;
      // graph grow
      double wkRatio =  (nLoads[i] - nLoads[i]/2) / (nLoads[i]/2);
      _grow_bisect(grid, i, wkRatio);
      // set ideal work
      double wkTot = _parts[i].work() + _parts[_tail].work();
      _parts[i].set_ideal_work(wkTot*wkRatio/(1.0+wkRatio));
      _parts[_tail].set_ideal_work(wkTot/(1.0+wkRatio));
      // exchange elements to reduce communication
      _exchange(grid, i, _tail);
      _parts.merge_part_blk(grid, i);
      _parts.merge_part_blk(grid, _tail);
      // adjust
      adjust_bisect(grid, i, _tail);
      _parts.merge_part_blk(grid, i);
      _parts.merge_part_blk(grid, _tail);
      nLoads.push_back(nLoads[i]/2);
      nLoads[i] -= nLoads[i] / 2;
    }
    // update non-empty partitions
    nPartUsed = _tail + 1;
    // clear for net iter
    _isMvds.clear();
    _msgDiffs0.clear();
    _msgDiffs1.clear();
  }

  _parts.merge_part_blk(grid);

  return(0);
}


int PartCluster::adjust_bisect(BlockGraph& bg, int pid0, int pid1)
{
  int      pidO, pidU; // overload, underload pid
  BlockCut cut;

  if(_parts[pid0].is_overload(_toler)){
    pidO = pid0;  pidU = pid1;
  }
  else if(_parts[pid1].is_overload(_toler)){
    pidO = pid1;  pidU = pid0;
  }
  else
    return(0);

  while(_parts[pidO].is_overload(_toler)){
    _parts.find_part_shift(bg, pidO, pidU, _toler, cut);
    if(cut.blkID != INDEX_NULL)
      _parts.shift_part(bg, pidU, cut);
    else 
      break;
  }

  return(0);
}


int PartCluster::_init_shmem_map(BlockGraph& bg)
{
  for(int i=0; i<bg.size(); i++){
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc = bg[i].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0) iter->isShMem = true;
        iter++;
      }
    }
  }

  return(0);
}


int PartCluster::_grow_bisect(BlockGraph& bg, int pid, double ratio)
{
  double targetWk = _parts[pid].work() / (1.0+ratio);
  double msgLoad;

  // init empty heap
  _hp0.clear();

  // mark blocks as undone
  for(unsigned int i=0; i<_parts[pid].blkIDs.size(); i++)
    _isMvds[_parts[pid].blkIDs[i]] = false;

  // check if there is empty partition
  if(_tail == _nPart){
    cerr << "ERROR: there is no empty partition to use." << endl;
    exit(-1);
  }

  // grow from seed and init heap
  int seed = _seed(bg, pid); 
  for(int iFc=0; iFc<6; iFc++){
    BlockFace *ptFc = bg[seed].face_pt(iFc);
    list<BcMap>::iterator iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        // if the block is in current partition and not in heap
        if(bg[iter->toBlk].partition_id() == pid && _hp0.find_blk(iter->toBlk) == INDEX_NULL){ 
          msgLoad = bg.msg_to_part(iter->toBlk, pid) - bg.msg_load(iter->toBlk, seed); 
          _hp0.add(iter->toBlk, msgLoad);
        }
      }
      iter++;
    }
  }
  // mv seed to new part
  _move_blk(bg, seed, pid, _tail);

  // grow the blocks in new part until exceed low bound of work
  while(_parts[_tail].work() < targetWk*(1.0 - _toler)){
    if(_hp0.size() == 0) break;
    // mv top block to new partition
    int topBlk = _hp0.pop();
    _move_blk(bg, topBlk, pid, _tail);
    // upadate the heap
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc = bg[topBlk].face_pt(iFc);
      list<BcMap>::iterator iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0){
          //if(!_isMvds[iter->toBlk] && iter->toBlk != topBlk){
          if(bg[iter->toBlk].partition_id() == pid){
            int pos = _hp0.find_blk(iter->toBlk);
            // if toBlk is not in heap then add it
            if(pos == INDEX_NULL){
              double msgLoad = bg.msg_to_part(iter->toBlk, pid) \
                             - bg.msg_to_part(iter->toBlk, _tail);
              _hp0.add(iter->toBlk, msgLoad);
            }
            // if toBlk is in heap then decrease its weight
            else{
              _hp0.decr(pos, 2.0*bg.msg_load(iter->toBlk, topBlk));
            }
          }
        }
        iter++;
      }
    }
  }

  return(0);
}


int PartCluster::_seed(BlockGraph& bg, int pid)
{
  int minB2Bblk = _parts[pid].blkIDs[0];
  int nB2Bmin   = 100;

  for(unsigned int i=0; i<_parts[pid].blkIDs.size(); i++){
    int nB2B = 0;
    // count b2b maps
    for(int iFc=0; iFc<6; iFc++){
      BlockFace  *ptFc;
      bg[_parts[pid].blkIDs[i]].get_face_pt(iFc, &ptFc);
      list<BcMap>::iterator iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0)  nB2B++;
        iter++;
      }
    }
    // update min # b2b and block id
    if(nB2B < nB2Bmin){
      nB2Bmin   = nB2B;
      minB2Bblk = _parts[pid].blkIDs[i];
    }
  }
  
  return(minB2Bblk);
}


int PartCluster::_exchange(BlockGraph& bg, int pid0, int pid1)
{
  int bid;                          // block id used later

  // clear both heaps
  _hp0.clear();
  _hp1.clear();

  // mark blocks in both partitions as undone
  for(unsigned int i=0; i<_parts[pid0].blkIDs.size(); i++)
    _isMvds[_parts[pid0].blkIDs[i]] = false;
  for(unsigned int i=0; i<_parts[pid1].blkIDs.size(); i++)
    _isMvds[_parts[pid1].blkIDs[i]] = false;

  // build up heaps for both partitions
  // heap for part0
  for(unsigned int i=0; i<_parts[pid0].blkIDs.size(); i++){
    bid             = _parts[pid0].blkIDs[i];
    _msgDiffs0[bid] = bg.msg_to_part(bid, pid0) - bg.msg_to_part(bid, pid1);
    if(_msgDiffs0[bid] <= 0)  _hp0.add(bid, _msgDiffs0[bid]);
  }
  // heap for part1
  for(unsigned int i=0; i<_parts[pid1].blkIDs.size(); i++){
    bid             = _parts[pid1].blkIDs[i];
    _msgDiffs1[bid] = bg.msg_to_part(bid, pid1) - bg.msg_to_part(bid, pid0);
    if(_msgDiffs1[bid] <= 0)  _hp1.add(bid, _msgDiffs1[bid]);
  }

#if verbose>=1
  cout << "PartCluster: ---- exchange blocks ----" << endl;
#endif

  // exchange blocks between two partitions
  int             pidFrom,       pidTo;
  ElementHeap    *ptHpFrom,     *ptHpTo;
  vector<double> *ptMsgDfsFrom, *ptMsgDfsTo;
  while(true){
    // set the shift direction
    // break if both heap empty
    if(_hp0.size() == 0 && _hp1.size() == 0){
      break;
    }
    // if one heap is empty, shift from the other
    else if(_hp0.size() == 0 && _hp1.size() > 0){
      pidFrom = pid1;  ptHpFrom = &_hp1;  ptMsgDfsFrom = &_msgDiffs1;
      pidTo   = pid0;  ptHpTo   = &_hp0;  ptMsgDfsTo   = &_msgDiffs0;
    }
    else if(_hp0.size() > 0  && _hp1.size() == 0){
      pidFrom = pid0;  ptHpFrom = &_hp0;  ptMsgDfsFrom = &_msgDiffs0;
      pidTo   = pid1;  ptHpTo   = &_hp1;  ptMsgDfsTo   = &_msgDiffs1;
    }
    // both not empty, shift from the min top to the other
    else{
      if(_cmpr_top(_hp0, _hp1) == -1){
        pidFrom = pid0;  ptHpFrom = &_hp0;  ptMsgDfsFrom = &_msgDiffs0;
        pidTo   = pid1;  ptHpTo   = &_hp1;  ptMsgDfsTo   = &_msgDiffs1;
      }
      else{
        pidFrom = pid1;  ptHpFrom = &_hp1;  ptMsgDfsFrom = &_msgDiffs1;
        pidTo   = pid0;  ptHpTo   = &_hp0;  ptMsgDfsTo   = &_msgDiffs0;
      }
    }

    // move the top block
    bid = ptHpFrom->pop();
    _move_blk(bg, bid, pidFrom, pidTo);

    // update both partiitons
    for(int iFc=0; iFc<6; iFc++){
      BlockFace* ptFc = bg[bid].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != bid){
          double msgDiff = 2.0 * bg.msg_load(iter->toBlk, bid);
          // if toBlk is in part0 (bid was)
          if(bg[iter->toBlk].partition_id() == pidFrom){
            // reduce msg load
            (*ptMsgDfsFrom)[iter->toBlk] -= msgDiff;
            // if toBlk is in heap
            int pos = ptHpFrom->find_blk(iter->toBlk);
            if(pos != INDEX_NULL)
              ptHpFrom->decr(pos, msgDiff);
            // if toBlk is not in heap
            else{
              if((*ptMsgDfsFrom)[iter->toBlk] < 0 && !_isMvds[iter->toBlk])
                ptHpFrom->add(iter->toBlk, (*ptMsgDfsFrom)[iter->toBlk]);
            }
          }
          else if(bg[iter->toBlk].partition_id() == pidTo){
            // increase msg load
            (*ptMsgDfsTo)[iter->toBlk] += msgDiff;
            // if toBlk is in heap
            int pos = ptHpTo->find_blk(iter->toBlk);
            if(pos != INDEX_NULL){
              if((*ptMsgDfsTo)[iter->toBlk] > 0)
                ptHpTo->pop(iter->toBlk);
              else
                ptHpTo->decr(iter->toBlk, -msgDiff);
            }
          }
        }
        iter++;
      }// end iter while
    }// end face loop

    // check the overload/underload error, exit if too large
    if(abs(_parts[pid0].imbalance()) > _tolerKL)  break;
    if(abs(_parts[pid1].imbalance()) > _tolerKL)  break;

  }// end outter while

  return(0);
}


int PartCluster::_move_blk(BlockGraph& bg, int bid, int pid0, int pid1)
{
  // delete block bid from part pid0
  _parts[pid0].del_blk(bid);
  _parts.add_load(pid0, -bg[bid].work_load());
  _parts[pid0].del_shmem_map(bg, bid);
  
  // set new part id
  bg[bid].set_partition(pid1);

  // Add block bid to part pid1
  _parts[pid1].blkIDs.push_back(bid);
  _parts.add_load(pid1, bg[bid].work_load());
  _parts[pid1].add_shmem_map(bg, bid);

  // mark block as moved
  _isMvds[bid] = true;

#if verbose>=2
  cout << "PartCluster: Move block " << bid << " from " 
       << pid0 << " to " << pid1 << endl;
#endif

  return(0);
}
