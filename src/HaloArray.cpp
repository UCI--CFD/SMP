#include "HaloArray.h"

using namespace std;

HaloArray::HaloArray(BlockGraph* ptBg, vector<int> bids, int nHalo)
{
  assert(bids.size() > 0);

  _ptBg   = ptBg;
  _bids   = bids;
  _nh     = nHalo;
  _nCopy  = 0;
  _cpyTimes  = NULL;
  _tShm   = 0.0;

  // allocate array and buffer
  int nCellSum     = 0;
  int nHaloCellSum = 0, nHaloCellMax = 0, nHaloCell;
  _blkStrts.push_back(0);
  for(uint i=0; i<_bids.size(); i++){
    nCellSum     += (*_ptBg)[bids[i]].num_cell(_nh);
    nHaloCell     = (*_ptBg)[bids[i]].num_halo_cell(_nh);
    nHaloCellSum += nHaloCell;
    if(nHaloCell > nHaloCellMax)  nHaloCellMax = nHaloCell;
    _blkStrts.push_back(nCellSum);
  }
  f         = new double [nCellSum];
  _sBuf     = new double [nHaloCellSum];
  _rBuf     = new double [nHaloCellSum];
  _shmemBuf = new double [nHaloCellMax];

  for(int i=0; i<nCellSum; i++)
    f[i] = 0.0;

  // subarray datatype
  _setup_comm();
}


HaloArray::HaloArray(const HaloArray& rhs)
{ 
  // rhs must have some data stored
  assert(rhs._blkStrts.back() > 0);

  _nh       = rhs._nh;
  _ptBg     = rhs._ptBg;
  _bids     = rhs._bids;
  _bufPoss  = rhs._bufPoss;
  _reqs     = rhs._reqs;
  _pids     = rhs._pids;
  _sTags    = rhs._sTags;
  _rTags    = rhs._rTags;
  _blkStrts = rhs._blkStrts;
  _tShm     = rhs._tShm;

  // allocate array and buffer
  f     = new double [_blkStrts.back()];
  for(int i=0; i<_blkStrts.back(); i++)
    f[i] = rhs.f[i];
  // ...
  int nHaloCell = 0;
  for(uint i=0; i<_bids.size(); i++)
    nHaloCell += (*_ptBg)[_bids[i]].num_halo_cell(_nh);
  _sBuf = new double [nHaloCell];
  _rBuf = new double [nHaloCell];
  for(int i=0; i<nHaloCell; i++){
    _sBuf[i] = rhs._sBuf[i];
    _rBuf[i] = rhs._rBuf[i];
  }
}


const HaloArray& HaloArray::operator=(const HaloArray& rhs)
{
  if(this != &rhs){
    _nh       = rhs._nh;
    _ptBg     = rhs._ptBg;
    _bids     = rhs._bids;
    _bufPoss  = rhs._bufPoss;
    _reqs     = rhs._reqs;
    _pids     = rhs._pids;
    _sTags    = rhs._sTags;
    _rTags    = rhs._rTags;
    _blkStrts = rhs._blkStrts;
    _tShm     = rhs._tShm;

    // allocate array and buffer
    delete [] f;
    f = new double [_blkStrts.back()];
    for(int i=0; i<_blkStrts.back(); i++)
      f[i] = rhs.f[i];
    // ...
    int nHaloCell = 0;
    for(uint i=0; i<_bids.size(); i++)
      nHaloCell += (*_ptBg)[_bids[i]].num_halo_cell(_nh);
    // ...
    _sBuf = new double [nHaloCell];
    _rBuf = new double [nHaloCell];
    for(int i=0; i<nHaloCell; i++){
      _sBuf[i] = rhs._sBuf[i];
      _rBuf[i] = rhs._rBuf[i];
    }
  }

  return(*this);
}


HaloArray::~HaloArray()
{
  delete [] f;
  delete [] _sBuf;
  delete [] _rBuf;
  delete [] _shmemBuf;

  if(_cpyTimes)  delete [] _cpyTimes;
}


double& HaloArray::at(int iBlk, int i, int j, int k)
{
  int rngs[6], iStep, jStep;

  (*_ptBg)[_bids[iBlk]].get_range(rngs);
  
  jStep = max(rngs[5] - rngs[2], 1) + 2*_nh;
  iStep = jStep * (rngs[4] - rngs[1] + 2*_nh);

  return(f[_blkStrts[iBlk] + i*iStep + j*jStep + k]);
}


void HaloArray::setup_timer(int nThrd)
{
  _nThrd    = nThrd;
  // _cpyTimes[0]        - time of direct copy in packing halo
  // _cpyTimes[_nThrd]   - time of indirect copy in packing halo
  // _cpyTimes[2*_nThrd] - time of copy in unpacking halo, all of them are direct
  _cpyTimes = new double [3*_nThrd];
  fill(_cpyTimes, _cpyTimes+3*_nThrd, 0.0);
}


void HaloArray::collect_time()
{
  double cpyTimeProc[3], imblncProc[3];

  for (int j=0; j<3; ++j) {
    double cpyTimeThrdMin = DLARGE, cpyTimeThrdMax = 0.0, cpyTimeThrdAvg = 0.0;
    for (int i=0; i<_nThrd; ++i) {
      cpyTimeThrdAvg += _cpyTimes[i+j*_nThrd];
      cpyTimeThrdMax  = max(cpyTimeThrdMax, _cpyTimes[i+j*_nThrd]);
      cpyTimeThrdMin  = min(cpyTimeThrdMin, _cpyTimes[i+j*_nThrd]);
    }
    cpyTimeThrdAvg = cpyTimeThrdAvg / _nThrd;
    imblncProc[j]  = (cpyTimeThrdMax - cpyTimeThrdMin) / (cpyTimeThrdAvg + DSMALL);
    cpyTimeProc[j] = cpyTimeThrdMax;
  }

  // collect max and avg time and imbalance
  int    nProc;
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);
  MPI_Allreduce(imblncProc,  _imblncMax,  3, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(cpyTimeProc, _cpyTimeMax, 3, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(imblncProc,  _imblncAvg,  3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(cpyTimeProc, _cpyTimeAvg, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  //..
  for (int j=0; j<3; ++j) {
    _imblncAvg[j]  = _imblncAvg[j]  / nProc;
    _cpyTimeAvg[j] = _cpyTimeAvg[j] / nProc;
  }
}


void HaloArray::view_copy_time()
{
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (rank == 0) {
    cout << "halo array copy time:" << endl;
    cout << "copyType\ttimeMax\timblncMax\ttimeAvg\timblncAvg" << endl;
    for (int j=0; j<3; ++j)
      cout << j << "\t" << _cpyTimeMax[j] << "\t" << _imblncMax[j] << "\t"
           << _cpyTimeAvg[j] << "\t" << _imblncAvg[j] << endl;
  }
}


int HaloArray::update_halo_begin()
{
  int    iComm = 0;
  int    rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  for(uint iBlk=0; iBlk<_bids.size(); iBlk++){// block
    for(int iFc=0; iFc<6; iFc++){// face
      BlockFace *ptFc = (*_ptBg)[_bids[iBlk]].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){// map
        // if b2b, need fetch data
        if(iter->bcType.compare("Block2Block") == 0 && !iter->isShMem){
          // send data
          int hlSize = max(iter->rngs[3] - iter->rngs[0], 1) \
                     * max(iter->rngs[4] - iter->rngs[1], 1) \
                     * max(iter->rngs[5] - iter->rngs[2], 1) * _nh;
          MPI_Isend(_sBuf+_bufPoss[iComm/2], hlSize, MPI_DOUBLE, _pids[iComm/2], \
                    _sTags[iComm/2], MPI_COMM_WORLD, &_reqs[iComm]);
          iComm++;
          // recv subarray type
          MPI_Irecv(_rBuf+_bufPoss[iComm/2], hlSize, MPI_DOUBLE, _pids[iComm/2], \
                    _rTags[iComm/2], MPI_COMM_WORLD, &_reqs[iComm]);
          iComm++;
        }
        iter++;
      }// while map
    }// for face 
  }// for block

  return(0);
}


int HaloArray::update_halo_end()
{
  MPI_Waitall((int)_reqs.size(), &_reqs[0], MPI_STATUSES_IGNORE);
  return(0);
}


int HaloArray::pack_halo_to_buf()
{
  int  rngs[6], iStrd, jStrd, bdRngs[6];
  int  iRmtMap = 0;

  for(uint iBlk=0; iBlk<_bids.size(); iBlk++){// block
    for(int iFc=0; iFc<6; iFc++){// face
      BlockFace *ptFc    = (*_ptBg)[_bids[iBlk]].face_pt(iFc);
      MapIter    iter    = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){// map
        if(iter->bcType.compare("Block2Block") == 0 && !iter->isShMem){
          // set block's range and strides
          (*_ptBg)[_bids[iBlk]].get_range(rngs);
          jStrd = max(rngs[5] - rngs[2], 1) + 2*_nh;
          iStrd = jStrd * (rngs[4] - rngs[1] + 2*_nh);
          // set the body part's range
          for(int i=0; i<3; i++){
            bdRngs[i]   = iter->rngs[i]   - rngs[i] + _nh;
            bdRngs[i+3] = iter->rngs[i+3] - rngs[i] + _nh;
          }
          if(iFc < 3){
            bdRngs[iFc]   =   _nh;
            bdRngs[iFc+3] = 2*_nh;
          }
          else{
            bdRngs[iFc-3] = rngs[iFc] - rngs[iFc-3];
            bdRngs[iFc]   = rngs[iFc] - rngs[iFc-3] + _nh;
          }
          // if no inversed, flipped, rotated axes
          if(iter->is_direct(iFc)){
            int    tid = omp_get_thread_num();
            double tmp = omp_get_wtime();
            int    jbdStride = bdRngs[5] - bdRngs[2];
            int    ibdStride = jbdStride*(bdRngs[4] - bdRngs[1]);
            #pragma omp for collapse(2) nowait
            for(int i=bdRngs[0]; i<bdRngs[3]; i++){
              for(int j=bdRngs[1]; j<bdRngs[4]; j++){
                memcpy(_sBuf+_bufPoss[iRmtMap]+(j-bdRngs[1])*jbdStride+(i-bdRngs[0])*ibdStride,
                       f+_blkStrts[iBlk]+i*iStrd+j*jStrd+bdRngs[2],
                       jbdStride*sizeof(double));
                //for(int k=bdRngs[2]; k<bdRngs[5]; k++){
                //int bufIdx = _bufPoss[iRmtMap] + (k-bdRngs[2]) \
                //+ (j-bdRngs[1])*(bdRngs[5]-bdRngs[2]) \
                //+ (i-bdRngs[0])*(bdRngs[5]-bdRngs[2])*(bdRngs[4]-bdRngs[1]);
                //_sBuf[bufIdx] = f[_blkStrts[iBlk] + i*iStrd + j*jStrd + k];
                  //}
              }
            }
            _cpyTimes[tid] += omp_get_wtime() - tmp;
          }
          else{
            // set the directions'mapping between source and target block
            int dirs[3]; // axes 0, 1, 2 mapped to target axes dirs[0, 1, 2]
            int toFcDir = iter->toFace % 3;
            dirs[iFc%3] = iter->toFace % 3;
            int dir0    = EdgeMap3d[iFc%3][0];
            int dir1    = EdgeMap3d[iFc%3][1];
            dirs[dir0]  = iter->isFlip ? EdgeMap3d[toFcDir][1] : EdgeMap3d[toFcDir][0];
            dirs[dir1]  = iter->isFlip ? EdgeMap3d[toFcDir][0] : EdgeMap3d[toFcDir][1];
            // check if axes are inversed
            int isInvs[3];
            isInvs[dir0]  = (iter->toRngs[dirs[dir0]+3] < iter->toRngs[dirs[dir0]]);
            isInvs[dir1]  = (iter->toRngs[dirs[dir1]+3] < iter->toRngs[dirs[dir1]]);
            if(iter->isFlip)
              isInvs[iFc%3] =  ( isInvs[dir0] &&  isInvs[dir1]) || (!isInvs[dir0] && !isInvs[dir1]);
            else
              isInvs[iFc%3] =  ( isInvs[dir0] && !isInvs[dir1]) || (!isInvs[dir0] &&  isInvs[dir1]);
            // set strides of the halo region.
            int hlLens[3];
            for(int i=0; i<3; i++)
              hlLens[i] = abs(iter->toRngs[i+3] - iter->toRngs[i]);
            hlLens[toFcDir] = _nh;
            int hlStrds[3] = {hlLens[1]*hlLens[2], hlLens[2], 1};
            // copy data
            int    tid = omp_get_thread_num();
            double tmp = omp_get_wtime();
            #pragma omp for collapse(3) nowait
            for(int i=bdRngs[0]; i<bdRngs[3]; i++){
              for(int j=bdRngs[1]; j<bdRngs[4]; j++){
                for(int k=bdRngs[2]; k<bdRngs[5]; k++){
                  int bufIdx = _bufPoss[iRmtMap];
                  bufIdx += ((1-isInvs[2])*(k-bdRngs[2]) + isInvs[2]*(bdRngs[5]-1-k)) * hlStrds[dirs[2]];
                  bufIdx += ((1-isInvs[1])*(j-bdRngs[1]) + isInvs[1]*(bdRngs[4]-1-j)) * hlStrds[dirs[1]];
                  bufIdx += ((1-isInvs[0])*(i-bdRngs[0]) + isInvs[0]*(bdRngs[3]-1-i)) * hlStrds[dirs[0]];
                  _sBuf[bufIdx] = f[_blkStrts[iBlk] + i*iStrd + j*jStrd + k];
                }
              }
            }
            _cpyTimes[tid+_nThrd] += omp_get_wtime() - tmp;
          }
          iRmtMap++;
        }
        iter++;
      }// end map
    }// end face
  }// end block

  return(0);
}


int HaloArray::unpack_buf_to_halo()
{
  int hlRngs[6], rngs[6], iStrd, jStrd;
  int iRmtMap = 0;

  for(uint iBlk=0; iBlk<_bids.size(); iBlk++){// block
    // set block range and stride
    (*_ptBg)[_bids[iBlk]].get_range(rngs);
    jStrd = max(rngs[5] - rngs[2], 1) + 2*_nh;
    iStrd = jStrd * (rngs[4] - rngs[1] + 2*_nh);
    // traverse face
    for(int iFc=0; iFc<6; iFc++){// face
      BlockFace *ptFc = (*_ptBg)[_bids[iBlk]].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){// map
        if(iter->bcType.compare("Block2Block") == 0 && !iter->isShMem){
          // set halo range, block range, stride
          for(int i=0; i<3; i++){
            hlRngs[i]    = iter->rngs[i]   - rngs[i] + _nh;
            hlRngs[i+3]  = iter->rngs[i+3] - rngs[i] + _nh;
          }
          if(iFc < 3){
            hlRngs[iFc]    =   0;
            hlRngs[iFc+3]  = _nh;
          }
          else{
            hlRngs[iFc-3] = rngs[iFc] - rngs[iFc-3] +   _nh;
            hlRngs[iFc]   = rngs[iFc] - rngs[iFc-3] + 2*_nh;
          }       
          // copy data from buffer, c order
          int    tid = omp_get_thread_num();
          double tmp = omp_get_wtime();
          int    jhlStride = hlRngs[5] - hlRngs[2];
          int    ihlStride = jhlStride * (hlRngs[4] - hlRngs[1]);
          #pragma omp for collapse(2) nowait
          for(int i=hlRngs[0]; i<hlRngs[3]; i++){
            for(int j=hlRngs[1]; j<hlRngs[4]; j++){
              memcpy(f+_blkStrts[iBlk]+i*iStrd+j*jStrd+hlRngs[2],\
                     _rBuf+_bufPoss[iRmtMap] + (j-hlRngs[1])*jhlStride + (i-hlRngs[0])*ihlStride,\
                     jhlStride*sizeof(double));
              //for(int k=hlRngs[2]; k<hlRngs[5]; k++){
                //int idx = _bufPoss[iRmtMap] + (k-hlRngs[2]) + (j-hlRngs[1])*(hlRngs[5]-hlRngs[2]) \
                //+ (i-hlRngs[0])*(hlRngs[5]-hlRngs[2])*(hlRngs[4]-hlRngs[1]);
                //f[_blkStrts[iBlk] + i*iStrd + j*jStrd + k] = _rBuf[idx];
                //}
            }
          }
          _cpyTimes[tid+2*_nThrd] += omp_get_wtime() - tmp;
          iRmtMap++;
        }
        iter++;
      }// end map
    }// end face
  }// end block

  return(0);
}


int HaloArray::copy_halo_shared_mem()
{
  int  rngs[6], toRngs[6], bdRngs[6], hlRngs[6]; // data copied from body to halo
  int  iStrd, jStrd, toStrds[3];

  for(uint iBlk=0; iBlk<_bids.size(); iBlk++){// block
    // set block's range and strides
    (*_ptBg)[_bids[iBlk]].get_range(rngs);
    jStrd = max(rngs[5] - rngs[2], 1) + 2*_nh;
    iStrd = jStrd * (rngs[4] - rngs[1] + 2*_nh);
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc  = (*_ptBg)[_bids[iBlk]].face_pt(iFc);
      MapIter    iter  = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){// map
        if(iter->bcType.compare("Block2Block") == 0 && iter->isShMem){
          // index of target block in bids
          int  iToBlk = vector_index(_bids, iter->toBlk);
          // set target block's range and strides
          (*_ptBg)[iter->toBlk].get_range(toRngs);
          toStrds[2] = 1;
          toStrds[1] = max(toRngs[5] - toRngs[2], 1) + 2*_nh;
          toStrds[0] = toStrds[1] * (toRngs[4] - toRngs[1] + 2*_nh);
          // set the body part's range, assume start index is (0,0,0), good for f
          for(int i=0; i<3; i++){
            bdRngs[i]   = iter->rngs[i]   - rngs[i] + _nh;
            bdRngs[i+3] = iter->rngs[i+3] - rngs[i] + _nh;
          }
          if(iFc < 3){
            bdRngs[iFc]   =   _nh;
            bdRngs[iFc+3] = 2*_nh;
          }
          else{
            bdRngs[iFc-3] = rngs[iFc] - rngs[iFc-3];
            bdRngs[iFc]   = rngs[iFc] - rngs[iFc-3] + _nh;
          }
          // set halo range, start index (0,0,0)
          for(int i=0; i<3; i++){
            hlRngs[i]   = iter->toRngs[i]   - toRngs[i] + _nh;
            hlRngs[i+3] = iter->toRngs[i+3] - toRngs[i] + _nh;
            if(hlRngs[i] > hlRngs[i+3])  swap(hlRngs[i], hlRngs[i+3]);
          }
          if(iter->toFace < 3){
            hlRngs[iter->toFace]   = 0;
            hlRngs[iter->toFace+3] = _nh;
          }
          else{
            hlRngs[iter->toFace-3] = toRngs[iter->toFace] - toRngs[iter->toFace-3] +   _nh;
            hlRngs[iter->toFace]   = toRngs[iter->toFace] - toRngs[iter->toFace-3] + 2*_nh;
          }
          // if no inversed, flipped, rotated axes
          if(iter->is_direct(iFc)){
            #pragma omp for collapse(2)  nowait
            for(int i=bdRngs[0]; i<bdRngs[3]; i++){
              for(int j=bdRngs[1]; j<bdRngs[4]; j++){
                memcpy(f + _blkStrts[iToBlk] + hlRngs[2] + (j-bdRngs[1]+hlRngs[1])*toStrds[1] + (i-bdRngs[0]+hlRngs[0])*toStrds[0],
                       f + _blkStrts[iBlk] + i*iStrd + j*jStrd + bdRngs[2],
                       (bdRngs[5] - bdRngs[2])*sizeof(double));
                //for(int k=bdRngs[2]; k<bdRngs[5]; k++){
                  //int toIdx = _blkStrts[iToBlk];
                  //toIdx   +=  k - bdRngs[2] + hlRngs[2];
                  //toIdx   += (j - bdRngs[1] + hlRngs[1]) * toStrds[1];
                  //toIdx   += (i - bdRngs[0] + hlRngs[0]) * toStrds[0];
                  //f[toIdx] = f[_blkStrts[iBlk] + i*iStrd + j*jStrd + k];
                //}
              }
            }
          }
          else{
            // set the directions'mapping between source and target block
            int dirs[3]; // axes 0, 1, 2 mapped to target axes dirs[0, 1, 2]
            int toFcDir = iter->toFace % 3;
            dirs[iFc%3] = iter->toFace % 3;
            int dir0    = EdgeMap3d[iFc%3][0];
            int dir1    = EdgeMap3d[iFc%3][1];
            dirs[dir0]  = iter->isFlip ? EdgeMap3d[toFcDir][1] : EdgeMap3d[toFcDir][0];
            dirs[dir1]  = iter->isFlip ? EdgeMap3d[toFcDir][0] : EdgeMap3d[toFcDir][1];
            // check if axes are inversed
            int isInvs[3];
            isInvs[dir0]  = (iter->toRngs[dirs[dir0]+3] < iter->toRngs[dirs[dir0]]);
            isInvs[dir1]  = (iter->toRngs[dirs[dir1]+3] < iter->toRngs[dirs[dir1]]);
            if(iter->isFlip)
              isInvs[iFc%3] =  ( isInvs[dir0] &&  isInvs[dir1]) || (!isInvs[dir0] && !isInvs[dir1]);
            else
              isInvs[iFc%3] =  ( isInvs[dir0] && !isInvs[dir1]) || (!isInvs[dir0] &&  isInvs[dir1]);
            // copy data
            #pragma omp for collapse(3) nowait
            for(int i=bdRngs[0]; i<bdRngs[3]; i++){
              for(int j=bdRngs[1]; j<bdRngs[4]; j++){
                for(int k=bdRngs[2]; k<bdRngs[5]; k++){
                  int toIdx = _blkStrts[iToBlk];
                  toIdx += ((1-isInvs[2])*(k - bdRngs[2] + hlRngs[dirs[2]]) \
                           +   isInvs[2] *(hlRngs[dirs[2]+3]-1 - k + bdRngs[2])) * toStrds[dirs[2]];
                  toIdx += ((1-isInvs[1])*(j - bdRngs[1] + hlRngs[dirs[1]]) \
                           +   isInvs[1] *(hlRngs[dirs[1]+3]-1 - j + bdRngs[1])) * toStrds[dirs[1]];
                  toIdx += ((1-isInvs[0])*(i - bdRngs[0] + hlRngs[dirs[0]]) \
                           +   isInvs[0] *(hlRngs[dirs[0]+3]-1 - i + bdRngs[0])) * toStrds[dirs[0]];
                  f[toIdx] = f[_blkStrts[iBlk] + i*iStrd + j*jStrd + k];
                }
              }
            }
          }
        }
        iter++;
      }// end map
    }// end face
  }// end block
        
  return(0);
}


int HaloArray::view()
{
  // print block info
  cout << "blocks: ";
  for(uint i=0; i<_bids.size(); i++)
    cout << _bids[i] << " ";
  cout << endl;

  // print communication info
  cout << "# shared mem copy:   " << _nCopy << endl;
  cout << "# MPI communication: " << _reqs.size()/2 << endl;

  return(0);
}


int HaloArray::_setup_comm()
{
  int iRmtMap = 0;

  // fisrt map's data starts at the beginning of buf
  _bufPoss.push_back(0);

  for(uint iBlk=0; iBlk < _bids.size(); iBlk++){
    for(int iFc=0; iFc<6; iFc++){
      BlockFace* ptFc = (*_ptBg)[_bids[iBlk]].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      int        iMap = 0;
      while(iter != ptFc->mapList.end()){
        // b2b, non-shared map requires communication
        if(iter->bcType.compare("Block2Block") == 0){
          // count shmem map
          if(iter->isShMem){
            _nCopy++;
          }
          else{
            // buf start index for this map
            int hlSize = max(iter->rngs[3] - iter->rngs[0], 1) \
                       * max(iter->rngs[4] - iter->rngs[1], 1) \
                       * max(iter->rngs[5] - iter->rngs[2], 1) * _nh;
            _bufPoss.push_back(_bufPoss[iRmtMap] + hlSize);
            // target
            _pids.push_back((*_ptBg)[iter->toBlk].partition_id());
            // send tags
            _sTags.push_back(_comm_tag(_bids[iBlk], iFc, iMap));
            // recv tags
            MapIter    iterInv = _ptBg->inv_map_iter(*iter);
            BlockFace *ptFcTo  = (*_ptBg)[iter->toBlk].face_pt(iter->toFace);
            int        iMapInv = distance(ptFcTo->mapList.begin(), iterInv);
            _rTags.push_back(_comm_tag(iter->toBlk, iter->toFace, iMapInv));
            // allocate requests, 2 for send & recv
            MPI_Request req;
            _reqs.push_back(req);
            _reqs.push_back(req);
            // increment b2bmap index
            iRmtMap++;
          }
        }
        iter++;
        iMap++;
      }
    }
  }

  return(0);
}


int HaloArray::_comm_tag(int bid, int fid, int mapid)
{
  // check input
  assert(bid   < 4194304);
  assert(mapid <     128);

  return((bid << 10) + (fid << 7) + mapid);
}
