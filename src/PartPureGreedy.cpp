#include "PartPureGreedy.h"

int PartPureGreedy::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
{
  grid = geo;

  // set up attributes
  _nPart    = nPart;
  //..
  double wkTot = 0.0;
  for(unsigned int i=0; i<bids.size(); i++)
    wkTot += (double)grid[bids[i]].work_load();
  _wkAvg = wkTot / _nPart;
#if verbose>=1
  cout << "PureGreedy: avg work Load " << _wkAvg << endl;
#endif

  // record wk per part
  _wkParts = new double [_nPart];
  fill(_wkParts, _wkParts+_nPart, 0.0);

  // makr all blocks undone, _isDones[i] matches bids[i]
  _isDones.clear();
  for(uint i=0; i<bids.size(); i++)
    _isDones.push_back(false);

  // greedy method variables
  int     bidMax = INDEX_NULL, iMax = INDEX_NULL, iPartMin = INDEX_NULL;
  int     nBlkDone = 0;
  int     nHalo    = grid.num_halo(), rngs[6];

  while(nBlkDone < (int)bids.size()){
    // find the largest undone block
    double wkMax = 0.0;
    for(uint i=0; i<bids.size(); i++){
      if(!_isDones[i] && grid[bids[i]].work() > wkMax){
        bidMax = bids[i];
        wkMax  = grid[bids[i]].work();
        iMax   = i;
      }
    }
    // find the most underload part
    double wkPartMin = DLARGE;
    for(int i=0; i<_nPart; i++){
      if(_wkParts[i] < wkPartMin){
        wkPartMin = _wkParts[i];
        iPartMin  = i;
      }
    }
    // if block exceeds load
    if(_wkParts[iPartMin] + grid[bidMax].work() > _wkAvg*(1.0+_toler)){
      // set long, mid , short direction and range
      int dirLong  = 0, dirShort = 0, dirMid = 0;
      grid[bidMax].get_axes_sorted(dirLong, dirMid, dirShort);
      grid[bidMax].get_range(rngs);
      // find the work load if cut at long edge
      int lenCut = (int)round((_wkAvg - _wkParts[iPartMin]) / grid[bidMax].area(dirLong));
      lenCut = max(lenCut, nHalo);
      lenCut = min(lenCut, grid[bidMax].length(dirLong)-nHalo);
      int wkCut  = lenCut * grid[bidMax].area(dirLong);
      // if cutting at long edge exceeds max toler,
      // search cut on both long and mid and find cut
      double errMin = abs(wkCut+_wkParts[iPartMin]-_wkAvg), err;
      int    posLong = INDEX_NULL, posMid = INDEX_NULL;
      if(errMin/_wkAvg > _toler){
        grid[bidMax].get_range(rngs);
        if(wkCut + _wkParts[iPartMin] > _wkAvg*(1.0+_tolerMax)){
          for(int iLong=nHalo; iLong<=grid[bidMax].length(dirLong)-nHalo; iLong++){
            for(int iMid=nHalo; iMid<=grid[bidMax].length(dirMid)-nHalo; iMid++){
              wkCut = iLong * iMid * grid[bidMax].length(dirShort);
              err   = abs(wkCut+_wkParts[iPartMin]-_wkAvg);
              if(err < errMin){
                errMin   = err;
                posLong  = rngs[dirLong] + iLong;
                posMid   = rngs[dirMid]  + iMid;
              }
            }
          }
        }
      }
      // if find proper cuts on long and mid edge, make 2way cut
      if(posLong != INDEX_NULL && posMid != INDEX_NULL){
        _cut_2way(grid, bids, bidMax, dirLong, posLong, dirMid, posMid);
#if verbose>=1
      cout << "******** 2 way cut ********" << endl;
      cout << "PureGreedy: cut max block " << bidMax << " in " << dirLong 
           << " " << dirMid << " off " << grid[bidMax].work();
      cout << "***************************" << endl;
#endif
        ++_n2Waycut;
      }
      // else cut at long is good enough of cut at both mid and long is worse
      else{
        _cut_1way(grid, bids, bidMax, dirLong, lenCut + rngs[dirLong]);
#if verbose>=1
      cout << "PureGreedy: cut max block " << bidMax << " in " << dirLong 
           << " off " << grid[bidMax].work();
#endif
        ++_n1WayCut;
      }
    }
    // if block fits in 
    else{
#if verbose>=1
      cout << "PureGreedy: max block " << bidMax;
#endif
    }

    grid[bidMax].set_partition(iPartMin);
    _wkParts[iPartMin] += grid[bidMax].work();
    _isDones[iMax] = true;
#if verbose >=1
    cout << " assigned to " << iPartMin << " " << _wkParts[iPartMin] << endl;
#endif

    nBlkDone++;
  }

  for(uint i=0; i<bids.size(); i++)
    grid.cmpt_time(bids[i]);

  delete [] _wkParts;

#if verbose>=1
  cout << "PartPureGreedy: # 1way cut " << _n1WayCut << " # 2way cut " 
       << _n2Waycut << endl;
#endif

  return(0);
}


int PartPureGreedy::_cut_2way(BlockGraph& bg, vector<int>& bids, int bid, int axis0,\
                              int pos0, int axis1, int pos1)
{
  BlockCut cut;

  // cut at axis0
  cut.blkID  = bid;
  cut.axis   = axis0;
  cut.pos    = pos0;
  cut.isKept = false;
  bg.cut_block(cut);
  bids.push_back(bg.size()-1);
  _isDones.push_back(false);

  // cut appending block at axis1
  cut.blkID  = bg.size()-1;
  cut.axis   = axis1;
  cut.pos    = pos1;
  bg.cut_block(cut);
  bids.push_back(bg.size()-1);
  _isDones.push_back(false);

  // cut remaining block at axis1
  cut.blkID  = bid;
  bg.cut_block(cut);
  bids.push_back(bg.size()-1);
  _isDones.push_back(false);

  return(0);
}



int PartPureGreedy::_cut_1way(BlockGraph& bg, vector<int>& bids, int bid, int axis, int pos)
{
  BlockCut cut;

  cut.set(bid, axis, pos, DLARGE, false);
  bg.cut_block(cut);
  bids.push_back(bg.size()-1);
  _isDones.push_back(false);

  return(0);
}
