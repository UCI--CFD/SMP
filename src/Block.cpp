#include "Block.h"

/*------------------------------------------------------------------------------
   BlockFace class member functions
------------------------------------------------------------------------------*/

BlockFace::BlockFace(const BlockFace& rhs)
{
  ID      = rhs.ID;
  nHalo   = rhs.nHalo;
  mapList = rhs.mapList;
  msgLoad = rhs.msgLoad;
}

const BlockFace& BlockFace::operator=(const BlockFace& rhs)
{
  if(this != &rhs){
    ID      = rhs.ID;
    nHalo   = rhs.nHalo;
    mapList = rhs.mapList;
    msgLoad = rhs.msgLoad;    
  }

  return(*this);
}

BlockFace::~BlockFace()
{
  mapList.clear();
}

// Add a map info for physical bc
int BlockFace::add_map(string boundaryType, const int ranges[6])
{
  BcMap map;

  // copy input
  map.bcType = boundaryType;
  memcpy(map.rngs, ranges, 6*sizeof(int));

  // set others with default value
  map.toBlk   = BLOCK_NULL;
  map.toFace  = FACE_NULL;
  map.isFlip  = false;   
  map.isShMem = false;
  fill(map.toRngs, map.toRngs+6, RANGE_NULL);

  // append to map list
  mapList.push_back(map);

  return(0);
}


// Add a map info
int BlockFace::add_map(string boundaryType, const int  ranges[6], int targetBlock, \
                       int    targetFace,   const int targetRanges[6], bool isMapFilp  )
{
  if(boundaryType.compare("Block2Block") != 0){
    cout << "ERROR: Try to add B2B map info to non B2B boundary" << endl;
    exit(-1);
  }

  // set map based on input
  BcMap map;
  map.bcType  = boundaryType;
  map.toBlk   = targetBlock;
  map.toFace  = targetFace;
  map.isFlip  = isMapFilp;
  map.isShMem = false;
  memcpy(map.rngs,   ranges,       6*sizeof(int));
  memcpy(map.toRngs, targetRanges, 6*sizeof(int));

  // append to map list
  mapList.push_back(map);

  return(0);
}


// view info
int BlockFace::view()
{
  cout << "# maps: " << mapList.size() << endl;

  list<BcMap>::iterator iter = mapList.begin();
  for(unsigned int i=0; i < mapList.size(); i++){
    // bc type
    cout.width(11);
    cout.flags(ios::left);
    cout << iter->bcType << "|";
    // range on source face
    for(int j=0; j<6; j++)
      cout << setw(6) << right << iter->rngs[j];
    cout << "|";
    // target block, face
    cout << setw(2) << right << iter->toBlk  << "|";
    cout << setw(2) << right << iter->toFace << "|";
    // range on target face
    for(int j=0; j<6; j++)
      cout << setw(6) << right << iter->toRngs[j] << " ";
    // shared or not
    cout << "|" << iter->isShMem;
    // flip or not
    cout << "|" << iter->isFlip << endl;

    iter++;
  }

  return(0);
}


int BlockFace::map_axis(const BcMap& map, int axis)
{
  if(axis == (ID%3)){
    cerr << "Error: BlockFace cannot map normal axis" << endl;
    exit(-1);
  }

  int iDir = 0; // 0 or 1
  if(axis == EdgeMap3d[ID%3][1])  iDir = 1;
  if(map.isFlip)                  iDir = 1 - iDir;

  return(EdgeMap3d[map.toFace%3][iDir]);
}


int BlockFace::map_axis_inv(const BcMap& map, int toAxis)
{
  int iDir = 0; // 0 or 1
  if(toAxis == EdgeMap3d[map.toFace%3][1])  iDir = 1;
  if(map.isFlip) iDir = 1 - iDir;

  return(EdgeMap3d[ID%3][iDir]);
}


int BlockFace::adjust_map_range(int side, int val, BcMap& map)
{
  int toAxis =  map_axis(map, side%3);
  
  if(map.toRngs[toAxis+3] < map.toRngs[toAxis]){
    if(side >= 3)
      map.toRngs[toAxis+3] = map.toRngs[toAxis] - val + map.rngs[side-3];
    else
      map.toRngs[toAxis] = map.toRngs[toAxis+3] - val + map.rngs[side+3];
  }
  else{
    if(side >= 3)
      map.toRngs[toAxis+3] = map.toRngs[toAxis] + val - map.rngs[side-3];
    else
      map.toRngs[toAxis] = map.toRngs[toAxis+3] + val - map.rngs[side+3];
  }

  map.rngs[side] = val;

  return(0);
}


int BlockFace::adjust_map_target_range(int side, int val, BcMap& map)
{
  int toAxis = side%3;
  int axis   = map_axis_inv(map, toAxis);
  
  if(map.toRngs[toAxis+3] < map.toRngs[toAxis]){
    if(side >= 3)
      map.rngs[axis+3] = map.rngs[axis] - val + map.toRngs[side-3];
    else
      map.rngs[axis] = map.rngs[axis+3] - val + map.toRngs[side+3];
  }
  else{
    if(side >= 3)
      map.rngs[axis+3] = map.rngs[axis] + val - map.toRngs[side-3];
    else
      map.rngs[axis] = map.rngs[axis+3] + val - map.toRngs[side+3];
  }

  map.toRngs[side] = val;

  return(0);
}


list<BcMap>::iterator BlockFace::map_iter(const BcMap& map)
{
  list<BcMap>::iterator iter = mapList.begin();
  while(iter != mapList.end()){
    if(memcmp(iter->rngs, map.rngs, 6*sizeof(int)) == 0)
      break;
    iter++;
  }
  return(iter);
}


int BlockFace::is_mergable(const BcMap& map0, const BcMap& map1) const
{
  if(map0.bcType.compare(map1.bcType) != 0)  return(0);

  if(map0.bcType.compare("Block2Block") == 0){
    if(map0.toBlk  != map1.toBlk )  return(0);
    if(map0.toFace != map1.toFace)  return(0);
  }

  int  dir0 = EdgeMap3d[ID%3][0];
  int  dir1 = EdgeMap3d[ID%3][1];

  if(map0.rngs[dir1] == map1.rngs[dir1] && map0.rngs[dir1+3] == map1.rngs[dir1+3]){
    if(map0.rngs[dir0]   == map1.rngs[dir0+3])  return(1);
    if(map0.rngs[dir0+3] == map1.rngs[dir0])    return(2);
  }

  if(map0.rngs[dir0] == map1.rngs[dir0] && map0.rngs[dir0+3] == map1.rngs[dir0+3]){
    if(map0.rngs[dir1]   == map1.rngs[dir1+3])  return(3);
    if(map0.rngs[dir1+3] == map1.rngs[dir1]  )  return(4);
  }
  
  return(0);
}


/*------------------------------------------------------------------------------
   Block class member functions
------------------------------------------------------------------------------*/


// constructor
Block::Block(int BlockID)
{
  blkID  = BlockID;
  pBlkID = BLOCK_NULL;
  x      = NULL;
  y      = NULL;
  z      = NULL;
  partID = PARTITION_NULL;

  for(int i=0; i<6; i++){
    rngs[i] = RANGE_NULL;
    faces[i].ID = i;
  }
}


// copy constructor
Block::Block(const Block& rhs)
{
  blkID   = rhs.blkID;
  pBlkID  = rhs.pBlkID;
  partID  = rhs.partID;
  msgLoad = rhs.msgLoad;
  ltncy   = rhs.ltncy;
  _t      = rhs._t;
  _tComp  = rhs._tComp;
  x       = NULL;
  y       = NULL;
  z       = NULL;

  for(int i=0; i<6; i++) {
    rngs[i]  = rhs.rngs[i];
    faces[i] = rhs.faces[i];
  }
}


// assignment
const Block& Block::operator=(const Block& rhs)
{
  if(this!=&rhs){
    blkID   = rhs.blkID;
    pBlkID  = rhs.pBlkID;
    partID  = rhs.partID;
    msgLoad = rhs.msgLoad;
    ltncy   = rhs.ltncy;
    _t      = rhs._t;
    _tComp  = rhs._tComp;

    for(int i=0; i<6; i++)
    {
      rngs[i]  = rhs.rngs[i];
      faces[i] = rhs.faces[i];
    }
  }

  return(*this);
}


// destructor
Block::~Block()
{
  // delete coordinate arrays
  if(x) delete [] x; x = NULL;
  if(y) delete [] y; y = NULL;
  if(z) delete [] z; z = NULL;
}


int Block::get_axes_sorted(int& dirLong, int& dirMid, int& dirShort) const
{
  dirLong  = 0;
  dirShort = 0;

  for(int i=0; i<3; ++i){
    if(length(i) > length(dirLong))   dirLong  = i;
    if(length(i) < length(dirShort))  dirShort = i;
  }

  if(dirLong == dirShort){
    dirLong  = 0;
    dirShort = 2;
  }

  dirMid = 3 - dirLong - dirShort;

  return(0);
}


int Block::num_cell(int nHalo) const
{
  return(  (rngs[3] - rngs[0] + 2*nHalo) * (rngs[4] - rngs[1] + 2*nHalo) \
         * (max(rngs[5] - rngs[2], 1) + 2*nHalo));
}


int Block::num_halo_cell(int nHalo) const
{
  int len[3];
  
  len[0] = rngs[3] - rngs[0] + 2*nHalo;
  len[1] = rngs[4] - rngs[1] + 2*nHalo;
  len[2] = max(rngs[5] - rngs[2], 1) + 2*nHalo;

  return(  2 * len[0] * len[1] * nHalo \
         + 2 * len[1] * len[2] * nHalo \
         + 2 * len[2] * len[0] * nHalo );
}


int Block::area(int dir) const
{
  int res;

  switch(dir%3){
    case 0:
      res = (rngs[4]-rngs[1])*max(rngs[5]-rngs[2], 1); break;
    case 1:
      res = (rngs[3]-rngs[0])*max(rngs[5]-rngs[2], 1); break;
    case 2:
      res = (rngs[3]-rngs[0])*(rngs[4]-rngs[1]); break;
  }

  return(res);
}


void Block::get_num_halo(int *nHalos)
{
  for(int i=0; i<6; i++)
    nHalos[i] = faces[i].nHalo;
}


void Block::get_coord_pt(int dir, double** ptCoord)
{
  switch(dir){
    case 0:
      *ptCoord = x; break;
    case 1:
      *ptCoord = y; break;
    case 2:
      *ptCoord = z; break;
  }
}


int Block::add_bc(int faceID, string boundaryType, const int ranges[6])
{
  faces[faceID].add_map(boundaryType, ranges);
  return(0);
}


int Block::add_bc(int  faceID,  string boundaryType, const int ranges[6],   \
                  int  toBlock, int    toFace,       const int toRanges[6], \
                  bool isFlip)
{
  faces[faceID].add_map(boundaryType, ranges, toBlock, toFace, toRanges, isFlip);
  return(0);
}


int Block::clear_bc(int fcID)
{
  faces[fcID].mapList.clear();
  return(0);
}


int Block::cut_off_bc(int side, const BlockCut& cut)
{
  int prllFcs[4];
  prllFcs[0] = EdgeMap3d[cut.axis][0];  prllFcs[2] = prllFcs[0] + 3;
  prllFcs[1] = EdgeMap3d[cut.axis][1];  prllFcs[3] = prllFcs[1] + 3;

  for(int j=0; j<4; j++){
    if(side == 0){
      list<BcMap>::iterator iter = faces[prllFcs[j]].mapList.begin();
      while(iter != faces[prllFcs[j]].mapList.end()){
        if(iter->rngs[cut.axis] >= cut.pos){
          iter = faces[prllFcs[j]].mapList.erase(iter);
          continue;
        }
        iter++;
      }
    }
    else if(side == 1){
      list<BcMap>::iterator iter = faces[prllFcs[j]].mapList.begin();
      while(iter != faces[prllFcs[j]].mapList.end()){
        if(iter->rngs[cut.axis+3] <= cut.pos){
          iter = faces[prllFcs[j]].mapList.erase(iter);
          continue;
        }
        iter++;
      }
    }
    else{
      cerr << "ERROR: Block: side is 0 or 1 "<< endl;
      exit(-1);
    }
  }

  return(0);
}


int Block::alloc_coord()
{
  int size = (rngs[3] - rngs[0] + 1 + faces[3].nHalo + faces[0].nHalo) \
           * (rngs[4] - rngs[1] + 1 + faces[4].nHalo + faces[1].nHalo) \
           * (rngs[5] - rngs[2] + 1 + faces[5].nHalo + faces[2].nHalo); 

  if(x) delete [] x;
  x = new double [size];

  if(y) delete [] y;
  y = new double [size];

  if(z) delete [] z;
  z = new double [size];

  return(0);
}


void Block::set_coord(int i, int j, int k, double coords[3])
{
  // stride
  int js =  rngs[5] - rngs[2] + 1 + faces[5].nHalo + faces[2].nHalo;
  int is = (rngs[4] - rngs[1] + 1 + faces[4].nHalo + faces[1].nHalo) * js;

  // index in 1D, set coordinates
  int idx = (i - (rngs[0] - faces[0].nHalo)) * is \
          + (j - (rngs[1] - faces[1].nHalo)) * js \
          +  k - (rngs[2] - faces[2].nHalo);
  x[idx]  = coords[0];
  y[idx]  = coords[1];
  z[idx]  = coords[2];
}


void Block::get_coord(int i, int j, int k, double coords[3])
{
  // stride
  int js =  rngs[5] - rngs[2] + 1 + faces[5].nHalo + faces[2].nHalo;
  int is = (rngs[4] - rngs[1] + 1 + faces[4].nHalo + faces[1].nHalo) * js;

  // index in 1D, set coordinates
  int idx = (i - (rngs[0] - faces[0].nHalo)) * is \
          + (j - (rngs[1] - faces[1].nHalo)) * js \
          +  k - (rngs[2] - faces[2].nHalo);
  coords[0] = x[idx];
  coords[1] = y[idx];
  coords[2] = z[idx];
}


int Block::copy_coord(const Block& blk)
{
  // start index, sizes, target start index, sizes
  int sizes[3], toSizes[3], starts[3], toStarts[3];
  for(int i=0; i<3; i++){
    sizes[i]    = rngs[i+3] - rngs[i] + 1 + faces[i+3].nHalo + faces[i].nHalo;
    starts[i]   = rngs[i]   - faces[i].nHalo;
    toSizes[i]  = blk.rngs[i+3] - blk.rngs[i] + 1 + blk.faces[i+3].nHalo + blk.faces[i].nHalo;
    toStarts[i] = blk.rngs[i]   - blk.faces[i].nHalo;
  }

  // copy coordinates
  for(int i = starts[0]; i < starts[0]+sizes[0]-1; i++){
    for(int j = starts[1]; j < starts[1]+sizes[1]-1; j++){
      for(int k = starts[2]; k < starts[2]+sizes[2]-1; k++){
        int idx   = (i - starts[0]) * sizes[1] * sizes[2] \
                  + (j - starts[1]) * sizes[2] +  k - starts[2];

        int toIdx = (i - toStarts[0]) * toSizes[1] * toSizes[2] \
                  + (j - toStarts[1]) * toSizes[2] + k - toStarts[2];

        x[idx] = blk.x[toIdx];
        y[idx] = blk.y[toIdx];
        z[idx] = blk.z[toIdx];
      }
    }
  }

  return(0);
}


int Block::view()
{
  cout << "Block    " << blkID  << endl;
  cout << "Parent   " << pBlkID << endl;
  cout << "Part     " << partID << endl;
  cout << "Ranges X " << rngs[0] << " - " << rngs[3] << endl;
  cout << "Ranges Y " << rngs[1] << " - " << rngs[4] << endl;
  cout << "Ranges Z " << rngs[2] << " - " << rngs[5] << endl;
  cout << "msg load " << msgLoad << endl;
  cout << "time     " << scientific << _t << endl;
  cout.unsetf(ios::scientific);
  cout << "Face info" << endl;
  for(int i=0; i<6; i++){
    faces[i].view();
  }

  return(0);
}


int Block::cmpt_msg_load(double alpha, double beta, int msgPerCell)
{
  msgLoad = 0.0;
  ltncy   = 0.0;

  for(int iFc=0; iFc<6; iFc++){
    // sum over maps to get face's msg load
    faces[iFc].msgLoad = 0.0; 
    list<BcMap>::iterator iter = faces[iFc].mapList.begin();
    while(iter != faces[iFc].mapList.end())
    {
      // b2b bc and not go through share memory
      if(iter->bcType.compare("Block2Block") == 0 && !iter->isShMem){
        faces[iFc].msgLoad += alpha;
        faces[iFc].msgLoad += max(iter->rngs[3] - iter->rngs[0], 1) \
                            * max(iter->rngs[4] - iter->rngs[1], 1) \
                            * max(iter->rngs[5] - iter->rngs[2], 1) \
                            * faces[iFc].nHalo * msgPerCell / beta;
        ltncy += alpha;
      }
      iter++; 
    }
    // sum over faces to get block's msg load
    msgLoad += faces[iFc].msgLoad;
  }

  return(0);
}


int Block::cmpt_time(double alpha, double beta, int sizePerCell, double tPerCell)
{
  cmpt_msg_load(alpha, beta, sizePerCell);

  _tComp = (rngs[3] - rngs[0])*(rngs[4] - rngs[1])*max(rngs[5] - rngs[2], 1) * tPerCell;
  _t     = _tComp + msgLoad;

  return(0);
}


double Block::time_comm(int fid, int pos, double alpha, double beta, int sizeCell)
/*----------------------------------------------------------
Note: This function is mainly used before performing a cut.
      Shmem map is added to communication time. This may cause
      error in time value when there is self map.
----------------------------------------------------------*/
{
  double tComm = 0.0;
  int    dir   = fid % 3, area;

  // set range
  assert(pos > rngs[dir] && pos < rngs[dir+3]);
  int    posS = (fid < 3) ? rngs[fid] : pos;        // start position
  int    posE = (fid < 3) ? pos       : rngs[fid];  // end   position

  // set parallel faces
  int pFcs[4];
  pFcs[0] = EdgeMap3d[dir][0];  pFcs[2] = pFcs[0] + 3;
  pFcs[1] = EdgeMap3d[dir][1];  pFcs[3] = pFcs[1] + 3;

  // add up communication time on parallel faces
  for(int i=0; i<4; i++){
    MapIter iter = faces[pFcs[i]].mapList.begin();
    while(iter != faces[pFcs[i]].mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        // if the b2b map intersects range
        if(iter->rngs[dir] < posE && iter->rngs[dir+3] > posS){
          area   = (min(posE, iter->rngs[dir+3]) - max(posS, iter->rngs[dir]))
                 * (iter->rngs[pFcs[1-i%2]+3] - iter->rngs[pFcs[1-i%2]]);
          tComm += area * faces[pFcs[i]].nHalo * sizeCell / beta + alpha;
        }
      }
      iter++;
    }
  }

  // add communication time of cut face
  area   = (rngs[pFcs[3]]-rngs[pFcs[1]]) * (rngs[pFcs[2]]-rngs[pFcs[0]]);
  tComm +=  area * faces[fid].nHalo * sizeCell / beta + alpha;

  // add communication time of the starting face
  MapIter iter = faces[fid].mapList.begin();
  while(iter != faces[fid].mapList.end()){
    if(iter->bcType.compare("Block2Block") == 0){
      area  = (iter->rngs[pFcs[3]] - iter->rngs[pFcs[1]]) \
            * (iter->rngs[pFcs[2]] - iter->rngs[pFcs[0]]);
      tComm += area * faces[fid%3].nHalo * sizeCell / beta + alpha;
    }
    iter++;
  }

  return(tComm);
}


bool Block::is_b2b_margin(int dir, int pos)
{
  int pFcs[4];
  pFcs[0] = EdgeMap3d[dir][0];  pFcs[2] = pFcs[0] + 3;
  pFcs[1] = EdgeMap3d[dir][1];  pFcs[3] = pFcs[1] + 3;

  for(int j=0; j<4; j++){
    MapIter iter = faces[pFcs[j]].mapList.begin();
    while(iter != faces[pFcs[j]].mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        if(pos == iter->rngs[dir] || pos == iter->rngs[dir+3]){
          return(true);
        }
      }
      iter++;
    }
  }

  return(false);
}
