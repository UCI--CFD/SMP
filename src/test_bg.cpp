#include <iostream>
#include "BlockGraph.h"

using namespace std;

int main()
{
  int rngs[6], toRngs[6];
  BlockGraph bg0(2);

  bg0.resize(1);

  Block *ptb;
  bg0.get_block_pt(0, &ptb);

  // set block data
  rngs[0] = 0;  rngs[3] = 112;
  rngs[1] = 0;  rngs[4] =  80;
  rngs[2] = 0;  rngs[5] =   0;
  ptb->set_range(rngs);

  // set block bc
  // x- bc
  rngs[0] =  0;  rngs[3] =  0;
  rngs[1] =  0;  rngs[4] = 80;
  ptb->add_bc(0, "FarField", rngs);
  // x+ bc
  rngs[0] = 112;  rngs[3] = 112;
  rngs[1] =   0;  rngs[4] =  80;
  ptb->add_bc(3, "FarField", rngs);
  // y- bc
  //.. 1st reverse
  rngs[0]   =   0; rngs[3]   = 32;
  rngs[1]   =   0; rngs[4]   =  0;
  toRngs[0] = 112; toRngs[3] = 80;
  toRngs[1] =   0; toRngs[4] =  0;
  toRngs[2] =   0; toRngs[5] =  0;
  ptb->add_bc(1, "Block2Block", rngs, 0, 1, toRngs, false);
  //.. wall
  rngs[0] = 32; rngs[3] = 80;
  ptb->add_bc(1, "EulerWal", rngs);
  //.. 2nd reverse
  rngs[0]   = 80; rngs[3]   = 112;
  toRngs[0] = 32; toRngs[3] =   0;
  ptb->add_bc(1, "Block2Block", rngs, 0, 1, toRngs, false);
  // y+ bc
  rngs[0] =  0;  rngs[3] = 112;
  rngs[1] = 80;  rngs[4] =  80;
  ptb->add_bc(4, "FarField", rngs);

  bg0.view();

  bg0.setup_msg_load(0.01, 10000.0, 1);

  BlockCut cut;
  bg0.find_min_blkcut(0, 96.0*80.0, 0.05, cut);
  cout << "cut axis: " << cut.axis    << endl;
  cout << "cut pos:  " << cut.pos     << endl;
  cout << "cut msg:  " << cut.msgIncr << endl;
  cout << "cut kept: " << cut.isKept  << endl;

  bg0.cut_block(cut);
  bg0.view();

  return(0);
}
