#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "GridInput.h"
#include "BlockGraph.h"
#include "BlockPart.h"
#include "PartMethod.h"
#include "Partitioner.h"
#include "HaloArray.h"

using namespace std;

int main(int argc, char* argv[])
{
  int         nProc, rank;
  BlockGraph  grid;
  vector<int> bids;
  GridInput   input;
  int         nh = 2;
  double      tComm[4], tComp[4], tCompCell[4], t[4], tmp, tCopy[4], tShr[4];
  double      *tCompPart, *tCommPart, *tCopyPart, *tShrPart;
  int         nRun = 512, nComp = 1;
  int         nCellPart = 0;
  string      meshName("mesh.x"), mapName("mapfile");

  MPI_Init(&argc, &argv);
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  // set method type from cmd line
  if(rank == 0){
    if(argc > 4){
      cerr << "Wrong number of command arguments" << endl;
      cerr << "The correct usage is ./[executable] (-file [mesh] [mapfile])" << endl;
    }
    else{
      for(int i=0; i<argc; i++){
        if(strcmp(argv[i], "-file") == 0)  meshName = argv[i+1];
        if(strcmp(argv[i], "-file") == 0)   mapName = argv[i+2];
      }
    }
  }

  input.bcast_block_info(meshName);
  input.bcast_map(mapName);

  grid.setup_msg_load(2.3081e-5, 1.7728e9, 8);
  grid.set_cell_time(1.1e-7);
  grid.read_input(input);

  // create and setup partitioner
  Partitioner pttnr(nProc);
  pttnr.setup_partition(grid);

  // evaluate partition properties
  if(rank == 0){
    pttnr.eval_part_prop(grid);
    pttnr.view_part_prop();
  }

  // create halo array
  pttnr.get_block_ids(rank, bids);
  HaloArray ha0(&grid, bids, nh), ha1(&grid, bids, nh);

  // init ha
  for(uint iBlk=0; iBlk<bids.size(); iBlk++){
    int rngs[6];
    grid[bids[iBlk]].get_range(rngs);
    nCellPart += grid[bids[iBlk]].num_cell(0);
    for(int i=nh; i<rngs[3]-rngs[0]+nh; i++){
      for(int j=nh; j<rngs[4]-rngs[1]+nh; j++){
        for(int k=nh; k<rngs[5]-rngs[2]+nh; k++){
          ha0.at((int)iBlk,i,j,k) = (i - nh + rngs[0]) * bids[iBlk] \
                                  +  j - nh + rngs[1] \
                                  + (double)(k - nh + rngs[2]) / (bids[iBlk]+1);
        }
      }
    }
  }

  // init timer
  fill(tComp, tComp+4, 0.0);
  fill(tComm, tComm+4, 0.0);
  fill(t,     t+4,     0.0);
  fill(tCopy, tCopy+4, 0.0);
  fill(tShr,  tShr+4,  0.0);
  tCommPart = new double [nProc];
  tCompPart = new double [nProc];
  tCopyPart = new double [nProc];
  tShrPart  = new double [nProc];
  fill(tCompPart, tCompPart+nProc, 0.0);
  fill(tCommPart, tCommPart+nProc, 0.0);
  fill(tCopyPart, tCopyPart+nProc, 0.0);
  fill(tShrPart,  tShrPart+nProc,  0.0);

  // solve
  for(int i=0; i<nRun; i++){
    int rngs[6], iStep, jStep, blkStart;
    // exchange halo
    tmp = MPI_Wtime();
    ha0.update_halo_begin();
    ha0.update_halo_end();
    tComm[0] += MPI_Wtime() - tmp;
    // compute
    tmp = MPI_Wtime();
    for(int j=0; j<nComp; j++){
      for(uint iBlk=0; iBlk<bids.size(); iBlk++){
        grid[bids[iBlk]].get_range(rngs);
        blkStart = ha0.block_start(iBlk);
        jStep    = max(rngs[5] - rngs[2], 1) + 2*nh;
        iStep    = jStep * (rngs[4] - rngs[1] + 2*nh);
        for(int i=nh; i<rngs[3]-rngs[0]+nh; i++){
          for(int j=nh; j<rngs[4]-rngs[1]+nh; j++){
            for(int k=nh; k<rngs[5]-rngs[2]+nh; k++){
              ha1.f[blkStart+i*iStep+j*jStep+k] = \
                ( 1.3 * ha0.f[blkStart+(i-1)*iStep+j*jStep+k] + 1.3 * ha0.f[blkStart+(i+1)*iStep+j*jStep+k] \
                + 0.8 * ha0.f[blkStart+i*iStep+(j-1)*jStep+k] + 0.8 * ha0.f[blkStart+i*iStep+(j+1)*jStep+k] \
                + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k-1]   + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k+1]   \
                + 1.3 * ha0.f[blkStart+(i-2)*iStep+j*jStep+k] + 1.3 * ha0.f[blkStart+(i+2)*iStep+j*jStep+k] \
                + 0.8 * ha0.f[blkStart+i*iStep+(j-2)*jStep+k] + 0.8 * ha0.f[blkStart+i*iStep+(j+2)*jStep+k] \
                + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k-2]   + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k+2] ) / 12.0;
            }
          }
        }
      }
    }
    tComp[0] += MPI_Wtime() - tmp;
    // exchange halo
    tmp = MPI_Wtime();
    ha1.update_halo_begin();
    ha1.update_halo_end();
    tComm[0] += MPI_Wtime() - tmp;
    // compute
    tmp = MPI_Wtime();
    for(int j=0; j<nComp; j++){
      for(uint iBlk=0; iBlk<bids.size(); iBlk++){
        grid[bids[iBlk]].get_range(rngs);
        blkStart = ha1.block_start(iBlk);
        jStep    = max(rngs[5] - rngs[2], 1) + 2*nh;
        iStep    = jStep * (rngs[4] - rngs[1] + 2*nh);
        for(int i=nh; i<rngs[3]-rngs[0]+nh; i++){
          for(int j=nh; j<rngs[4]-rngs[1]+nh; j++){
            for(int k=nh; k<rngs[5]-rngs[2]+nh; k++){
              ha0.f[blkStart+i*iStep+j*jStep+k] = \
                ( 1.3 * ha1.f[blkStart+(i-1)*iStep+j*jStep+k] + 1.3 * ha1.f[blkStart+(i+1)*iStep+j*jStep+k] \
                + 0.8 * ha1.f[blkStart+i*iStep+(j-1)*jStep+k] + 0.8 * ha1.f[blkStart+i*iStep+(j+1)*jStep+k] \
                + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k-1]   + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k+1]   \
                + 1.3 * ha1.f[blkStart+(i-2)*iStep+j*jStep+k] + 1.3 * ha1.f[blkStart+(i+2)*iStep+j*jStep+k] \
                + 0.8 * ha1.f[blkStart+i*iStep+(j-2)*jStep+k] + 0.8 * ha1.f[blkStart+i*iStep+(j+2)*jStep+k] \
                + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k-2]   + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k+2] ) / 12.0;
            }
          }
        }
      }
    }
    tComp[0] += MPI_Wtime() - tmp;
  }

  // collect time
  t[0]     = tComp[0] + tComm[0];
  tCopy[0] = ha0.time_copy_mpi() + ha1.time_copy_mpi();
  tShr[0]  = ha0.time_shmem()    + ha1.time_shmem();

  // min, avg, max time
  MPI_Allreduce(tComp, tComp+1, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(tComp, tComp+2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(tComp, tComp+3, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(tComm, tComm+1, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(tComm, tComm+2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(tComm, tComm+3, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(tCopy, tCopy+1, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(tCopy, tCopy+2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(tCopy, tCopy+3, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(tShr,  tShr+1,  1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(tShr,  tShr+2,  1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(tShr,  tShr+3,  1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  MPI_Allreduce(t,     t+1,     1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(t,     t+2,     1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(t,     t+3,     1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  tComp[2] = tComp[2] / nProc;
  tComm[2] = tComm[2] / nProc;
  t[2]     = t[2]     / nProc;
  tCopy[2] = tCopy[2] / nProc;
  tShr[2]  = tShr[2]  / nProc;

  // min, avg, max comp time per cell
  tCompCell[0] = tComp[0] / nCellPart;
  MPI_Allreduce(tCompCell, tCompCell+1, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(tCompCell, tCompCell+2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(tCompCell, tCompCell+3, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  tCompCell[2] = tCompCell[2] / nProc;

  // collect time from all processes
  MPI_Gather(tComp, 1, MPI_DOUBLE, tCompPart, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Gather(tComm, 1, MPI_DOUBLE, tCommPart, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Gather(tCopy, 1, MPI_DOUBLE, tCopyPart, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Gather(tShr , 1, MPI_DOUBLE, tShrPart,  1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // print time
  if(rank == 0){
    // head line
    cout << "time\troot\tmin\tavg\tmax" << endl;
    cout << scientific;
    // total time
    cout << "total ";
    for(int i=0; i<4; i++)
      cout << t[i] /2/nRun << " ";
    cout << endl;
    // computation time
    cout << "comp ";
    for(int i=0; i<4; i++)
      cout << tComp[i]/2/nRun << " ";
    cout << endl;
    // communication time
    cout << "comm ";
    for(int i=0; i<4; i++)
      cout << tComm[i]/2/nRun << " ";
    cout << endl;
    // copy time
    cout << "copy ";
    for(int i=0; i<4; i++)
      cout << tCopy[i]/2/nRun << " ";
    cout << endl;
    // share time
    cout << "shr ";
    for(int i=0; i<4; i++)
      cout << tShr[i]/2/nRun << " ";
    cout << endl;
    // computation time per cell
    cout << "comp cell ";
    for(int i=0; i<4; i++)
      cout << tCompCell[i]/2/nRun << " ";
    cout << endl;
    
    // write file of time cost of each part
    size_t found = meshName.find_last_of("/");
    string fname = meshName.substr(found+1);
    fname.erase(fname.end()-2, fname.end());
    fname.append("_time.txt");
    fname.insert(0,"./results/");
    ofstream output(fname);
    output << "part\tcomp\tcomm\tcopy\tShr\ttotal" << endl;
    for(int i=0; i<nProc; i++)
      output << i << "\t" << tCompPart[i]/2/nRun << "\t" 
             << tCommPart[i]/2/nRun << "\t" 
             << tCopyPart[i]/2/nRun << "\t"
             << tShrPart[i]/2/nRun  << "\t"
             << (tCompPart[i] + tCommPart[i])/2/nRun << endl;
    output.close();
  }

  delete [] tCompPart;
  delete [] tCommPart;
  delete [] tCopyPart;
  delete [] tShrPart;

  MPI_Finalize();

  return(0);
}
