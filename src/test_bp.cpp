#include <stdlib.h>
#include <iostream>
#include "BlockGraph.h"
#include "BlockPart.h"

using namespace std;

const int    NX         = 113;
const int    NY         =  81;
const int    WALL_START =  32;
const int    WALL_END   =  80;
const double LX         = 7.0;
const double LY         = 5.0;

int main(int argc, char* argv[])
{
  if(argc != 2){
    cerr << "Wrong number of command arguments.\n" \
         << "The correct usage is ./[executable] [#proc]." << endl;
    exit(-1);
  }

  int         nProc = atoi(argv[1]);
  int         rngs[6], toRngs[6];
  BlockGraph  geo(1);
  Block       *ptb;

  geo.get_block_pt(0, &ptb);

  // set block data range
  rngs[0] =  0;  rngs[3] = NX-1;
  rngs[1] =  0;  rngs[4] = NY-1;
  rngs[2] =  0;  rngs[5] =  0;
  ptb->set_range(rngs);

  // set block bc
  // x- bc
  rngs[0] =  0;  rngs[3] =  0;
  rngs[1] =  0;  rngs[4] =  NY-1;
  ptb->add_bc(0, "FarField", rngs);
  // x+ bc
  rngs[0] = NX-1;  rngs[3] = NX-1;
  rngs[1] =    0;  rngs[4] = NY-1;
  ptb->add_bc(3, "FarField", rngs);
  // y- bc 1
  rngs[0]   = 0;     rngs[3]   = WALL_START;
  rngs[1]   = 0;     rngs[4]   = 0;
  toRngs[0] = NX-1;  toRngs[3] = WALL_END;
  toRngs[1] = 0;     toRngs[4] = 0;
  toRngs[2] = 0;     toRngs[5] = 0;
  ptb->add_bc(1, "Block2Block", rngs, 0, 1, toRngs, false);
  // y- bc 2
  rngs[0] = WALL_START;  rngs[3] = WALL_END;
  rngs[1] = 0;           rngs[4] = 0;
  ptb->add_bc(1, "EulerWall", rngs);
  // y- bc 3
  rngs[0]   = WALL_END;    rngs[3]   = NX-1;
  rngs[1]   = 0;           rngs[4]   = 0;
  toRngs[0] = WALL_START;  toRngs[3] = 0;
  toRngs[1] = 0;           toRngs[4] = 0;
  toRngs[2] = 0;           toRngs[5] = 0;
  ptb->add_bc(1, "Block2Block", rngs, 0, 1, toRngs, false);
  // y+ bc
  rngs[0] = 0;     rngs[3] = NX-1;
  rngs[1] = NY-1;  rngs[4] = NY-1;
  ptb->add_bc(4, "FarField", rngs);

  // save a copy
  //BlockGraph grid(geo);
  geo.setup_msg_load(0.01, 10000.0, 1);

  // set coordinates for geo
  double dx = LX / (double)(NX-1);
  double dy = LY / (double)(NY-1);
  ptb->get_range(rngs);
  ptb->alloc_coord();
  for(int i=rngs[0]-1; i<=rngs[3]+1; i++){
    for(int j=rngs[1]-1; j<=rngs[4]+1; j++){
      for(int k=rngs[2]-1; k<=rngs[5]+1; k++){
        double coords[3];
        coords[0] = i * dx;
        coords[1] = j * dy;
        coords[2] = 0.0;
        ptb->set_coord(i, j, k, coords);
      }
    }
  }

  // partition
  BlockGraph grid;
  BlockPart bp(nProc);
  bp.dcmps(geo, grid);
  bp.view_part_prop();

  // write the decomp file
  grid.fwrite_mesh_tecplot("decomp.plt");

  return(0);
}
