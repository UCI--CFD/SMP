#include "common.h"

using namespace std;

bool is_prime(int n)
{
  bool isDivided = false;
  int  factorCl  = (int)ceil(sqrt(n));

  for(int i=2; i<factorCl; i++){
    isDivided = (isDivided || (n%i == 0));
  }

  return(!isDivided);
}


int vector_index(vector<int>& intVec, int val)
{
  vector<int>::iterator iter = find(intVec.begin(), intVec.end(), val);

  if(iter == intVec.end()){
    return(INDEX_NULL);
  }
  else{
    return((int)distance(intVec.begin(), iter));
  }
}
