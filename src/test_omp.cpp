#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <omp.h>
#include "GridInput.h"
#include "BlockGraph.h"
#include "BlockPart.h"
#include "PartMethod.h"
#include "Partitioner.h"
#include "HaloArray.h"

using namespace std;

int divide_thread(int nThrd, int l0, int l1, int& n0, int& n1)
{
  n0 = max((int)round(sqrt((double)l0*nThrd/l1)), 1);
  n0 = min(nThrd, n0);
  int flr = n0, cl = n0;
  for(flr=n0; flr>0; flr--)
    if(nThrd%flr == 0)  break;
  for(cl=n0; cl<=nThrd; cl++)
    if(nThrd%cl  == 0)  break;
  n0 = (n0 - flr < cl - n0) ? flr : cl;
  n1 = nThrd / n0;

  return(0);
}

int set_thread_range(int rngs[6], int nThrd, int tid, int tRngs[6])
{
  int l[3] = {rngs[3] - rngs[0], rngs[4] - rngs[1], rngs[5] - rngs[2]};
  int n[3], t[3];

  // divide among x, y
  divide_thread(nThrd, l[0], l[1], n[0], n[1]);  n[2] = 1;
  // lx is not long enough, divide among y, z
  if(n[0] > l[0]){
    divide_thread(nThrd, l[1], l[2], n[1], n[2]);
    n[0] = 1;
  }
  // ly is not long enough, divide among x, z
  if(n[1] > l[1]){
    divide_thread(nThrd, l[0], l[2], n[0], n[2]);
    n[1] = 1;
  }
  assert(n[0] <= l[0] && n[1] <= l[1] && n[2] <= l[2]);

  // set thread's index range
  t[0] = tid / n[1] / n[2];
  t[1] = tid % (n[1]*n[2]) / n[2];
  t[2] = tid % n[2];
  for(int i=0; i<3; i++){
    int stepFlr = l[i] / n[i];
    int nFlr    = n[i] - l[i]%n[i];
    tRngs[i]    = rngs[i] + t[i]*stepFlr + max(0, t[i] - nFlr);
    tRngs[i+3]  = (t[i] >= nFlr) ? tRngs[i]+stepFlr+1 : tRngs[i]+stepFlr;
  }

  return(0);
}

int main(int argc, char* argv[])
{
  int         nProc, rank, thrdMode;
  BlockGraph  geo, grid;
  vector<int> bids;
  int         nh = 2;
  double      t[28], *tComp_t, *tCopy_t, *tShr_t, *tPart;
  int         nRun = 1, nComp = 4;
  int         nCellPart = 0;
  int         nThrd = 2;
  string      testName("fh_pc256");
  int         methodType = 0, adjustType = INDEX_NULL;
  string      meshName("mesh.x"), mapName("mapfile");
  char        charBuf[10];

  MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &thrdMode);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  if(thrdMode != MPI_THREAD_SERIALIZED){
    if(rank == 0) cerr << "Does not support MPI_THREAD_SERIALIZED" << endl;
    MPI_Finalize();
  }

  // set method type from cmd line
  if(rank == 0){
    //if(argc > 7){
      //cerr << "Wrong number of command arguments" << endl;
      //cerr << "The correct usage is ./[executable] (-method [type] "
           //<< "-adjust [type] -testname [testName])"
           //<< endl;
           //}
           //else {
      for (int i=0; i<argc; i++) {
        if (strcmp(argv[i], "-method") == 0)   methodType = atoi(argv[i+1]);
        if (strcmp(argv[i], "-adjust") == 0)   adjustType = atoi(argv[i+1]);
        if (strcmp(argv[i], "-testname") == 0) testName   = argv[i+1];
        if (strcmp(argv[i], "-file") == 0)     meshName   = argv[i+1];
        if (strcmp(argv[i], "-file") == 0)     mapName    = argv[i+2];
        if (strcmp(argv[i], "-ncomp") == 0)    nComp      = atoi(argv[i+1]);
      }
      //}
  }
  // bcast method type
  MPI_Bcast(&methodType, 1, MPI_INT, 0, MPI_COMM_WORLD);
  // bcast adjust type
  MPI_Bcast(&adjustType, 1, MPI_INT, 0, MPI_COMM_WORLD);
  // bcast #computation per step
  MPI_Bcast(&nComp,      1, MPI_INT, 0, MPI_COMM_WORLD);
  // bcase file name
  if (rank == 0)  strcpy(charBuf, meshName.c_str());
  MPI_Bcast(charBuf, 10, MPI_CHARACTER, 0, MPI_COMM_WORLD);
  if (rank != 0) meshName = charBuf;
  strcpy(charBuf, meshName.c_str());
  if (rank == 0)  strcpy(charBuf, mapName.c_str());
  MPI_Bcast(charBuf, 10, MPI_CHARACTER, 0, MPI_COMM_WORLD);
  if (rank != 0)  mapName = charBuf;

  geo.setup_msg_load(1.7261e-5, 1.7736e9, 8);
  geo.set_cell_time(1.4e-8);
  geo.fread_mesh_plot3d(meshName, false);
  geo.fread_map(mapName);

  // create and setup partitioner
  Partitioner pttnr(nProc);
  pttnr.set_tolerance(0.05);
  pttnr.setup_partition_method((Method_t)methodType);

  // decompose the geometry blocks
  pttnr.decompose(geo, grid);

  // adjust the grid if set to
  if (adjustType != INDEX_NULL) {
    if (adjustType == 0) pttnr.adjust_all(grid);
    if (adjustType == 1) pttnr.adjust_greedy(grid);
  }

  // evaluate partition properties
  if(rank == 0){
    grid.debug_check();
    pttnr.eval_part_prop(grid);
    pttnr.view_part_prop();
    cout << " #blocks " << grid.size() << endl;
  }

  // create halo array
  pttnr.get_block_ids(rank, bids);
  HaloArray ha0(&grid, bids, nh), ha1(&grid, bids, nh);
  ha0.setup_timer(nThrd);
  ha1.setup_timer(nThrd);

  // init ha
  for(uint iBlk=0; iBlk<bids.size(); iBlk++){
    int rngs[6];
    grid[bids[iBlk]].get_range(rngs);
    nCellPart += grid[bids[iBlk]].num_cell(0);
    for(int i=nh; i<rngs[3]-rngs[0]+nh; i++){
      for(int j=nh; j<rngs[4]-rngs[1]+nh; j++){
        for(int k=nh; k<rngs[5]-rngs[2]+nh; k++){
          ha0.at((int)iBlk,i,j,k) = (i - nh + rngs[0]) * bids[iBlk] \
                                  +  j - nh + rngs[1] \
                                  + (double)(k - nh + rngs[2]) / (bids[iBlk]+1);
        }
      }
    }
  }

  // init timer
  // 0 total, 1 comp, 2 comm, 3 copy, 4 share, 5 spin, 6 cell
  fill(t, t+28, 0.0);
  tPart   = new double [6*nProc];
  tComp_t = new double [nThrd];  fill(tComp_t,tComp_t+nThrd, 0.0);
  tCopy_t = new double [nThrd];  fill(tCopy_t,tCopy_t+nThrd, 0.0);
  tShr_t  = new double [nThrd];  fill(tShr_t, tShr_t +nThrd, 0.0);

  MPI_Barrier(MPI_COMM_WORLD);

  t[0] = MPI_Wtime();

#pragma omp parallel num_threads(nThrd)
{
  int    tid = omp_get_thread_num();
  int    rngs[6], tRngs[6], blkStart, iStep, jStep;
  double tmp, tmpMaster;
  // solve
  for(int iRun=0; iRun<nRun; iRun++){
  // copy halo data to buffer for MPI
    tmp = omp_get_wtime();
    ha0.pack_halo_to_buf();
    tCopy_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier  
    // MPI communicate + copy via shared mem
    tmp = omp_get_wtime();
    #pragma omp master
    {
      tmpMaster = MPI_Wtime();
      ha0.update_halo_begin();
      ha0.update_halo_end();
      t[2] += MPI_Wtime() - tmpMaster;
    }
    ha0.copy_halo_shared_mem();
    tShr_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    // copy from buffer to halo
    tmp = omp_get_wtime();
    ha0.unpack_buf_to_halo();
    tCopy_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    // compute
    tmp = omp_get_wtime();
    for(int iComp=0; iComp<nComp; iComp++){
      for(uint iBlk=0; iBlk<bids.size(); iBlk++){
        grid[bids[iBlk]].get_range(rngs);
        set_thread_range(rngs, nThrd, tid, tRngs);
        blkStart = ha0.block_start(iBlk);
        jStep    = max(rngs[5] - rngs[2], 1) + 2*nh;
        iStep    = jStep * (rngs[4] - rngs[1] + 2*nh);
        for(int i=tRngs[0]-rngs[0]+nh; i<tRngs[3]-rngs[0]+nh; i++){
          for(int j=tRngs[1]-rngs[1]+nh; j<tRngs[4]-rngs[1]+nh; j++){
            for(int k=tRngs[2]-rngs[2]+nh; k<tRngs[5]-rngs[2]+nh; k++){
              ha1.f[blkStart+i*iStep+j*jStep+k] = \
                ( 1.3 * ha0.f[blkStart+(i-1)*iStep+j*jStep+k] + 1.3 * ha0.f[blkStart+(i+1)*iStep+j*jStep+k] \
                + 0.8 * ha0.f[blkStart+i*iStep+(j-1)*jStep+k] + 0.8 * ha0.f[blkStart+i*iStep+(j+1)*jStep+k] \
                + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k-1]   + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k+1]   \
                + 1.3 * ha0.f[blkStart+(i-2)*iStep+j*jStep+k] + 1.3 * ha0.f[blkStart+(i+2)*iStep+j*jStep+k] \
                + 0.8 * ha0.f[blkStart+i*iStep+(j-2)*jStep+k] + 0.8 * ha0.f[blkStart+i*iStep+(j+2)*jStep+k] \
                + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k-2]   + 0.9 * ha0.f[blkStart+i*iStep+j*jStep+k+2] ) / 12.0;
            }
          }
        }
      }
    }
    tComp_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    // copy halo data to buffer for MPI
    tmp = omp_get_wtime();
    ha1.pack_halo_to_buf();
    tCopy_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    // MPI communicate + copy via shared mem
    tmp = omp_get_wtime();
    #pragma omp master
    {
      tmpMaster = MPI_Wtime();
      ha1.update_halo_begin();
      ha1.update_halo_end();
      t[2] += MPI_Wtime() - tmpMaster;
    }
    ha1.copy_halo_shared_mem();
    tShr_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    tmp = omp_get_wtime();
    ha1.unpack_buf_to_halo();
    tCopy_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
    // compute
    tmp = omp_get_wtime();
    for(int j=0; j<nComp; j++){
      for(uint iBlk=0; iBlk<bids.size(); iBlk++){
        grid[bids[iBlk]].get_range(rngs);
        set_thread_range(rngs, nThrd, tid, tRngs);
        blkStart = ha1.block_start(iBlk);
        jStep    = max(rngs[5] - rngs[2], 1) + 2*nh;
        iStep    = jStep * (rngs[4] - rngs[1] + 2*nh);
        for(int i=tRngs[0]-rngs[0]+nh; i<tRngs[3]-rngs[0]+nh; i++){
          for(int j=tRngs[1]-rngs[1]+nh; j<tRngs[4]-rngs[1]+nh; j++){
            for(int k=tRngs[2]-rngs[2]+nh; k<tRngs[5]-rngs[2]+nh; k++){
              ha0.f[blkStart+i*iStep+j*jStep+k] = \
                ( 1.3 * ha1.f[blkStart+(i-1)*iStep+j*jStep+k] + 1.3 * ha1.f[blkStart+(i+1)*iStep+j*jStep+k] \
                + 0.8 * ha1.f[blkStart+i*iStep+(j-1)*jStep+k] + 0.8 * ha1.f[blkStart+i*iStep+(j+1)*jStep+k] \
                + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k-1]   + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k+1]   \
                + 1.3 * ha1.f[blkStart+(i-2)*iStep+j*jStep+k] + 1.3 * ha1.f[blkStart+(i+2)*iStep+j*jStep+k] \
                + 0.8 * ha1.f[blkStart+i*iStep+(j-2)*jStep+k] + 0.8 * ha1.f[blkStart+i*iStep+(j+2)*jStep+k] \
                + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k-2]   + 0.9 * ha1.f[blkStart+i*iStep+j*jStep+k+2] ) / 12.0;
            }
          }
        }
      }
    }
    tComp_t[tid] += omp_get_wtime() - tmp;
    #pragma omp barrier
  }
}// end parallel

  // save time
  // total time
  t[0] = MPI_Wtime() - t[0];
  // pick max thread time
  t[1] = *max_element(tComp_t, tComp_t+nThrd);
  t[3] = *max_element(tCopy_t, tCopy_t+nThrd);
  t[4] = *max_element(tShr_t,  tShr_t +nThrd) - t[2];
  // spin time
  t[5] = t[0] - t[1] - t[2] - t[3] - t[4];
  // time per cell, exclude communication
  t[6] = (t[0] - t[2])/nCellPart;

  // collect min, avg, max time
  MPI_Allreduce(t, t+ 7, 7, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(t, t+14, 7, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(t, t+21, 7, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  for(int i=14; i<21; i++)
    t[i] = t[i] / nProc;

  // collect time from all processes
  MPI_Gather(t, 6, MPI_DOUBLE, tPart, 6, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  // print time
  if(rank == 0){
    // head line
    cout << "time\troot\tmin\tavg\tmax" << endl;
    cout << scientific;
    // total time
    cout << "total ";
    for(int i=0; i<4; i++)
      cout << t[7*i]/2/nRun << " ";
    cout << endl;
    // computation time
    cout << "comp ";
    for(int i=0; i<4; i++)
      cout << t[7*i+1]/2/nRun << " ";
    cout << endl;
    // communication time
    cout << "comm ";
    for(int i=0; i<4; i++)
      cout << t[7*i+2]/2/nRun << " ";
    cout << endl;
    //spin time
    cout << "spin ";
    for(int i=0; i<4; i++)
      cout << t[7*i+5]/2/nRun << " ";
    cout << endl;
    // copy time
    cout << "copy ";
    for(int i=0; i<4; i++)
      cout << t[7*i+3]/2/nRun << " ";
    cout << endl;
    // share time
    cout << "shr ";
    for(int i=0; i<4; i++)
      cout << t[7*i+4]/2/nRun << " ";
    cout << endl;
    // computation time per cell
    cout << "cell ";
    for(int i=0; i<4; i++)
      cout << t[7*i+6]/2/nRun << " ";
    cout << endl;
    
    // write file of time cost of each part
    testName.append("_time.txt");
    testName.insert(0,"./results/");
    ofstream output(testName);
    output << "part\tcomp\tcomm\tspin\tcopy\tshr\ttotal" << endl;
    for(int i=0; i<nProc; i++)
      output << i << "\t" << tPart[6*i+1]/2/nRun << "\t" << tPart[6*i+2]/2/nRun << "\t" 
             << tPart[6*i+5]/2/nRun << "\t" << tPart[6*i+3]/2/nRun << "\t"
             << tPart[6*i+4]/2/nRun << "\t" << tPart[6*i]/2/nRun << endl;
    output.close();
  }

  // collect and print copy time
  ha0.collect_time();
  ha1.collect_time();
  ha0.view_copy_time();
  ha1.view_copy_time();


  delete [] tPart;
  delete [] tComp_t;
  delete [] tCopy_t;
  delete [] tShr_t;

  MPI_Finalize();

  return(0);
}
