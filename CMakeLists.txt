cmake_minimum_required(VERSION 2.8)

project(SMP)
enable_language(CXX)

if(CMAKE_CXX_COMPILER_ID MATCHES GNU)
  set(CMAKE_CXX_FLAGS "-std=c++11 -fmax-errors=3 -fopenmp -Wall -Wextra -O3 -Dverbose=0")
elseif(CMAKE_CXX_COMPILER_ID MATCHES Intel)
  set(CMAKE_CXX_FLAGS "-std=c++11 -diag-error-limit=3 -qopenmp -warn -O3 -Dverbose=0")
endif()

find_path(metisInc metis.h HINTS "$ENV{METIS_ROOT}/include")
find_library(metisLib NAMES metis HINTS "$ENV{METIS_ROOT}/lib")
include_directories(${metisInc})

include_directories(include)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/bin")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/lib")

add_subdirectory(src)
